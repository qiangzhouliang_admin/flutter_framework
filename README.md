# flutter_framework
- flutter sdk版本：2.8.1，对应分支：FS2.8.1
- flutter sdk版本：3.0.0，对应分支：FS3.0.0
- flutter sdk版本：3.7.1，对应分支：FS3.7.1
- flutter sdk版本：3.7.11，对应分组:FS3.7.11
- flutter sdk版本：3.10.3，对应分组：FS3.10.3
- flutter sdk版本：3.27.0，对应分组：FS3.27.0

一个flutter2.0开发框架,支持Android、iOS、web、macOS、Windows等各个平台的开发
# 封装功能如下

# 注意事项
## 1.1 ota_update 使用注意
1. Android
升级版本号后，需要将文件android/src/main/res/xml/filepaths.xml内容替换为以下内容。
```xml
<?xml version="1.0" encoding="utf-8"?>
<paths xmlns:android="http://schemas.android.com/apk/res/android">
    <files-path name="internal_apk_storage" path="ota_update/"/>
</paths>
```
将以下提供程序引用添加到<application>节点内的 AndroidManifest.xml 。
```
<provider
    android:name="sk.fourq.otaupdate.OtaUpdateFileProvider"
    android:authorities="${applicationId}.ota_update_provider"
    android:exported="false"
    android:grantUriPermissions="true">
    <meta-data
        android:name="android.support.FILE_PROVIDER_PATHS"
        android:resource="@xml/filepaths" />
</provider>
```
出于安全原因，Android 下载管理器默认禁用明文流量。要允许它，您需要创建文件res/xml/network_security_config.xml
```xml
<?xml version="1.0" encoding="utf-8"?>
<network-security-config>
    <base-config cleartextTrafficPermitted="true" />
</network-security-config>
```
并AndroidManifest.xml在应用程序标签中引用它
```xml
android:networkSecurityConfig="@xml/network_security_config"
```

