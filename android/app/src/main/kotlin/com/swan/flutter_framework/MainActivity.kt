package com.swan.flutter_framework

import android.os.Bundle
import io.flutter.plugin.common.MethodChannel
import io.flutter.embedding.android.FlutterActivity

class MainActivity : FlutterActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        //FlutterMain.startInitialization(this);
        super.onCreate(savedInstanceState)

       // GeneratedPluginRegistrant.registerWith(FlutterEngine(this)) // here is the error: Type mismatch. Required: FlutterEngine! Found: MainActivity

        //接收第三方app调运参数并且传递给flutter
        flutterEngine?.dartExecutor?.let {
            MethodChannel(it, CHANNEL).setMethodCallHandler { call, result ->
                if (call.method == InvokeMethod) {
                    val greetings = successNativeCode()
                    result.success(greetings)
                }
            }
        }
    }

    private fun successNativeCode(): String {
        return "我是android原生跑过来的数据=>${intent.getStringExtra("test")}"
    }

    companion object {
        private const val CHANNEL = "com.swan.shareData"
        private const val InvokeMethod = "shareData"
    }
}
