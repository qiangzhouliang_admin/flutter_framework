/// @author 强周亮(qiangzhouliang)
/// @desc 分页实体
/// @email 2538096489@qq.com
/// @time 2020/8/19 13:58
class PageModel {
  int? pageSize;
  int? start;
  int? totalCount;
  int? firstPage;
  int? nextPage;
  int? lastPage;
  int? prevPage;
  int? currentPageNo;
  int? totalPageCount;

  PageModel({this.pageSize, this.start, this.totalCount, this.firstPage, this.nextPage, this.lastPage, this.prevPage, this.currentPageNo, this.totalPageCount});

  PageModel.fromJson(Map<String, dynamic> json) {    
    this.pageSize = json['pageSize'];
    this.start = json['start'];
    this.totalCount = json['totalCount'];
    this.firstPage = json['firstPage'];
    this.nextPage = json['nextPage'];
    this.lastPage = json['lastPage'];
    this.prevPage = json['prevPage'];
    this.currentPageNo = json['currentPageNo'];
    this.totalPageCount = json['totalPageCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['pageSize'] = this.pageSize;
    data['start'] = this.start;
    data['totalCount'] = this.totalCount;
    data['firstPage'] = this.firstPage;
    data['nextPage'] = this.nextPage;
    data['lastPage'] = this.lastPage;
    data['prevPage'] = this.prevPage;
    data['currentPageNo'] = this.currentPageNo;
    data['totalPageCount'] = this.totalPageCount;
    return data;
  }

}
