class UserModel {
  String? message;
  int? code;
  DataBean? data;

  UserModel({this.message, this.code, this.data});

  UserModel.fromJson(Map<String, dynamic> json) {    
    message = json['message'];
    code = json['code'];
    data = (json['data'] != null ? DataBean.fromJson(json['data']) : null);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['message'] = message;
    data['code'] = code;
    if (this.data != null) {
      data['data'] = this.data?.toJson();
    }
    return data;
  }

}

class DataBean {
  String? areaCode;
  String? orgCode;
  bool? isXfbm;
  UserInfoBean? userInfo;

  DataBean({this.areaCode, this.orgCode, this.isXfbm, this.userInfo});

  DataBean.fromJson(Map<String, dynamic> json) {    
    areaCode = json['areaCode'];
    orgCode = json['orgCode'];
    isXfbm = json['isXfbm'];
    userInfo = json['userInfo'] != null ? UserInfoBean.fromJson(json['userInfo']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['areaCode'] = areaCode;
    data['orgCode'] = orgCode;
    data['isXfbm'] = isXfbm;
    if (userInfo != null) {
      data['userInfo'] = userInfo?.toJson();
    }
    return data;
  }
}

class UserInfoBean {
  String? id;
  String? name;
  String? loginName;
  String? mobile;
  CompanyBean? company;
  List<RoleListListBean>? roleList;
  List<RolesListBean>? roles;

  UserInfoBean({this.id, this.name, this.loginName, this.mobile, this.company, this.roleList, this.roles});

  UserInfoBean.fromJson(Map<String, dynamic> json) {    
    id = json['id'];
    name = json['name'];
    loginName = json['loginName'];
    mobile = json['mobile'];
    company = json['company'] != null ? CompanyBean.fromJson(json['company']) : null;
    roleList = (json['roleList'] as List)!=null?(json['roleList'] as List).map((i) => RoleListListBean.fromJson(i)).toList():null;
    roles = (json['roles'] as List)!=null?(json['roles'] as List).map((i) => RolesListBean.fromJson(i)).toList():null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['loginName'] = loginName;
    data['mobile'] = mobile;
    if (company != null) {
      data['company'] = company?.toJson();
    }
    data['roleList'] = roleList != null?roleList?.map((i) => i.toJson()).toList():null;
    data['roles'] = roles != null?roles?.map((i) => i.toJson()).toList():null;
    return data;
  }
}

class CompanyBean {
  String? id;
  String? name;

  CompanyBean({this.id, this.name});

  CompanyBean.fromJson(Map<String, dynamic> json) {    
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    return data;
  }
}

class RoleListListBean {
  String? userId;
  String? roleId;

  RoleListListBean({this.userId, this.roleId});

  RoleListListBean.fromJson(Map<String, dynamic> json) {    
    this.userId = json['userId'];
    this.roleId = json['roleId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userId'] = this.userId;
    data['roleId'] = this.roleId;
    return data;
  }
}

class RolesListBean {
  String? id;
  String? name;
  String? enname;

  RolesListBean({this.id, this.name, this.enname});

  RolesListBean.fromJson(Map<String, dynamic> json) {    
    this.id = json['id'];
    this.name = json['name'];
    this.enname = json['enname'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['enname'] = this.enname;
    return data;
  }
}
