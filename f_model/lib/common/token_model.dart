class TokenModel {
  String? accessToken;
  String? refreshToken;
  String? tokenType;
  int? expiresIn;

  TokenModel({this.accessToken, this.refreshToken, this.tokenType, this.expiresIn});

  TokenModel.fromJson(Map<String, dynamic> json) {    
    this.accessToken = json['access_token'];
    this.refreshToken = json['refresh_token'];
    this.tokenType = json['token_type'];
    this.expiresIn = json['expires_in'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['access_token'] = this.accessToken;
    data['refresh_token'] = this.refreshToken;
    data['token_type'] = this.tokenType;
    data['expires_in'] = this.expiresIn;
    return data;
  }

}
