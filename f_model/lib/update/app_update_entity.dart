class AppUpdateEntity {
  String? version;
  String? isForceUpdate;
  String? apkSize;
  String? description;
  String? updateTime;

  AppUpdateEntity({
      this.version, 
      this.isForceUpdate, 
      this.apkSize, 
      this.description, 
      this.updateTime});

  AppUpdateEntity.fromJson(dynamic json) {
    version = json["version"];
    isForceUpdate = json["isForceUpdate"];
    apkSize = json["apkSize"];
    description = json["description"];
    updateTime = json["updateTime"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["version"] = version;
    map["isForceUpdate"] = isForceUpdate;
    map["apkSize"] = apkSize;
    map["description"] = description;
    map["updateTime"] = updateTime;
    return map;
  }

}