import 'package:f_base/base_exp.dart';
import 'package:flutter/material.dart';
import 'package:m_pc/web/bean/BlogBean.dart';
/// 菜单controller
class MenuController extends GetxController{

  @override
  void onInit(){
    super.onInit();
    updateBlogList(_currentSelectIndex.value);
  }
  //首页左滑菜单控制key
  GlobalKey<ScaffoldState> _homeScaffoldKey = GlobalKey();
  GlobalKey<ScaffoldState> get homeScaffoldKey => _homeScaffoldKey;

  // 当前那个菜单被选中
  RxInt _currentSelectIndex = 0.obs;
  RxInt get currentSelectIndex => _currentSelectIndex;
  void updateSelectIndex(int index) {
    _currentSelectIndex.value = index;
    //更新列表数据
    updateBlogList(index);
  }

  //菜单列表
  final List<String> _nemuList = [
    "首页",
    "Flutter",
    "Android",
    "Ios",
    "Java",
    "前端",
    "关于我们",
  ];

  List<String> get menuList => _nemuList;


  //当前显示类目的文章数据
  RxList<BlogBean> _blogList = <BlogBean>[].obs;
  RxList get blogList => _blogList;
  //存储不同分类的文章列表
  Map<int,List<BlogBean>> map = {};

  void updateBlogList(int index) async {
    //从map中获取数据
    List<BlogBean>? list = map[index];
    if(list == null || list.length == 0){
      list = [];
      //每次加载前，清空数据
      _blogList.value = [];
      //模拟网络请求
      await Future.delayed(const Duration(seconds: 1));
      for (int i = 0; i < 10; i++) {
        list.add(BlogBean("${_nemuList[index]}-$i",Res.app_update_titleImg));
      }

      map[index] = list;
    }

    _blogList.value = list;
  }

}