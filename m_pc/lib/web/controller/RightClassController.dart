import 'package:f_base/base_exp.dart';
/// 右侧分类controller
class RightClassController extends GetxController{
  //菜单分类
  final List<String> _classList = [
    "Android从入门到精通","Flutter 零基础教程","java开发中的点滴积累","程序员的每日学习"
  ];

  List<String> get classList => _classList;

  /// 专家解读
  final List<String> _personList = [
    "早起的年轻人","每日学习"
  ];

  List<String> get personList => _personList;
}