import 'package:flutter/material.dart';

class HeaderIconWidget extends StatelessWidget {
  //图片路径
  String path;

  //点击事件
  Function()? onTab;

  HeaderIconWidget(this.path, {this.onTab});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTab,
      child: Container(
        margin: const EdgeInsets.only(right: 16),
        child: Image.asset(path,width: 22,),
      ),
    );
  }
}
