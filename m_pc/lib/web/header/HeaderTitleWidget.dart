import 'package:flutter/material.dart';

class HeaderTitleWidget extends StatelessWidget {
  final String title;
  final bool isSelected;
  final Function()? onTab;

  const HeaderTitleWidget(this.title,this.isSelected, {this.onTab});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTab,
      child: Container(
        padding: const EdgeInsets.only(left: 12,right: 12),
        child: Text(
          title,
          style: TextStyle(
              fontWeight: FontWeight.w500, fontSize: 22, color: isSelected ? Colors.red: Colors.white),
        ),
      ),
    );
  }
}
