import 'package:f_base/base_exp.dart';
import 'package:flutter/material.dart';
import 'package:m_pc/web/controller/RightClassController.dart';

import 'RightClassWidget.dart';
import 'SearchWidget.dart';
/// 右侧内容页
class RightContentWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => RightContentWidgetState();
}

class RightContentWidgetState extends State<RightContentWidget> {
  final RightClassController _rightClassController =
  Get.put(RightClassController());
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          //第一行的搜索
          SearchWidget(),
          //第二行的分类
          RightClassWidget(title:"分类",classList: _rightClassController.classList),
          //第三行的专家解读
          RightClassWidget(title:"专家解读",classList: _rightClassController.personList),
        ],
      ),
    );
  }
}
