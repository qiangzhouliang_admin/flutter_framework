import 'package:flutter/material.dart';
import 'package:m_pc/web/bean/BlogBean.dart';
/// 左侧类容条目
class LeftContentItemWidget extends StatelessWidget{
  final BlogBean blogBean;
  LeftContentItemWidget(this.blogBean);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 12),
      decoration: const BoxDecoration(color: Colors.white,borderRadius: BorderRadius.all(Radius.circular(10))),
      child: Column(
        children: [
          Image.asset(blogBean.imagePath!,height: 400,),
          const SizedBox(height: 16,),
          Text(blogBean.title!,style: const TextStyle(fontWeight: FontWeight.w500,fontSize: 18),)
      ],),
    );
  }

}