import 'package:f_base/base_exp.dart';
import 'package:flutter/material.dart';
import 'package:m_pc/web/bean/BlogBean.dart';
import 'package:m_pc/web/controller/MenuController.dart' as MC;

import 'LeftContentItemWidget.dart';

/// 左侧文章列表
class LeftContentWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LeftContentWidgetState();
  }
}

class LeftContentWidgetState extends State<LeftContentWidget> {
  final MC.MenuController _menuController = Get.put(MC.MenuController());

  @override
  Widget build(BuildContext context) {
    return Obx((){
      return buildListView();
    });
  }

  Widget buildListView() {
    //获取数据
    List<BlogBean> list = _menuController.blogList as List<BlogBean>;
    if(list.isEmpty){
      return Text("加载中。。。");
    }
    return ListView.builder(
    //禁止滑动
    physics: const NeverScrollableScrollPhysics(),
    //包裹
    shrinkWrap: true,
    itemCount: list.length,
    itemBuilder: (BuildContext context, int index) {
      //获取每一个条目对应的数据
      BlogBean _blogBean = list[index];
      return LeftContentItemWidget(_blogBean);
    });
  }
}
