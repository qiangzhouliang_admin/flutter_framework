import 'package:f_base/base_exp.dart';
import 'package:flutter/material.dart';

import 'LeftContentWidget.dart';
import 'RightContentWidget.dart';
///内容部分
class ContentWidget extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return ContentWidgetState();
  }
  
}
class ContentWidgetState extends State<ContentWidget>{
  @override
  Widget build(BuildContext context) {
    //屏幕宽度适配
    if(CommonUtils.getScreenWidth(context) <= 840){
      return Container(
        //设置margin值
        margin: const EdgeInsets.all(12),
        child: LeftContentWidget(),
      );
    }
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        //左侧的文章列表
        Expanded(flex:2,child: LeftContentWidget()),
        //右侧的分类列表
        Expanded(flex:1,child: RightContentWidget())
      ],);
  }
  
}