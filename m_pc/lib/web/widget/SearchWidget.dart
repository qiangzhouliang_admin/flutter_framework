import 'package:flutter/material.dart';

/// 搜索组件
class SearchWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.all(22),
      padding: const EdgeInsets.all(12),
      // 圆角边框
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        //左对齐
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            "搜搜",
            style: TextStyle(fontWeight: FontWeight.w600),
          ),
          //输入框
          Container(
            //上边距
            margin: const EdgeInsets.only(top: 12),
            //左边距
            padding: const EdgeInsets.only(left: 12),
            // 圆角边框
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(12)),
              border: Border.all(color: Colors.grey,width: 1.0),
            ),
            child: Row(
              children: const [
                Expanded(
                  child: TextField(
                    decoration: InputDecoration(hintText: "请输入要搜素的内容",border: InputBorder.none),
                  ),
                ),
                Icon(Icons.search)
              ],
            ),
          ),
        ],
      ),
    );
  }
}
