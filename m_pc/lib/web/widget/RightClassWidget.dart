import 'package:flutter/material.dart';

/// 右边分类组件
class RightClassWidget extends StatelessWidget {

  final String title;
  final List<String> classList;


  RightClassWidget({required this.title, required this.classList});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      //外边距
      margin: const EdgeInsets.all(22),
      //内边距
      padding: const EdgeInsets.all(12),
      // 圆角边框
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        //左对齐
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(fontWeight: FontWeight.w600),
          ),
          ListView(
            //包裹
            shrinkWrap: true,
            children: [
              ...classList.map((e) => ListTile(
                    title: Text(e),
                  ))
            ],
          ),
        ],
      ),
    );
  }
}
