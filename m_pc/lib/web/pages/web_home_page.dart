import 'package:f_base/base_exp.dart';
import 'package:flutter/material.dart';
import 'package:m_pc/web/controller/MenuController.dart' as MC;
import 'package:m_pc/web/header/header_widget.dart';
import 'package:m_pc/web/menu/SlideMenuWidget.dart';
import 'package:m_pc/web/widget/content_widget.dart';
/// web端首页
class WebHomePage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return WebHomePageState();
  }
  
}
class WebHomePageState extends State<WebHomePage>{
  // 绑定注入控制器
  final MC.MenuController _menuController = Get.put(MC.MenuController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //定义一个key，控制侧拉页面的展开与收缩
      key: _menuController.homeScaffoldKey,
      backgroundColor: Colors.grey[200],
      //侧拉页面
      drawer: SlideMenuWidget(),
      /// 页面整体滑动
      body: SingleChildScrollView(
        child: Column(children: [
          // 第一部分标题
          HeaderWidget(),
          //第二部分内容
          Container(
            constraints: BoxConstraints(maxWidth: 1000),
            child: ContentWidget(),
          )
        ],),
      ),
    );
  }
  
}