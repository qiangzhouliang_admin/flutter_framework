class BlogBean {
  //文章标题
  String? title;
  //文章图片
  String? imagePath;

  BlogBean(this.title, this.imagePath);
}