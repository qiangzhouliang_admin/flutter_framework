import 'package:f_base/base_exp.dart';
import 'package:flutter/material.dart';
import 'package:m_pc/web/controller/MenuController.dart' as MC;

/// 左侧拉菜单
class SlideMenuWidget extends StatelessWidget {
  // 绑定注入控制器
  final MC.MenuController _menuController = Get.put(MC.MenuController());

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Colors.black,
        child: Obx(() {
          return buildListView();
        }),
      ),
    );
  }

  ListView buildListView() {
    return ListView(
      children: [
        const DrawerHeader(
            child: Text(
          "早起的年轻人",
          style: TextStyle(
              fontWeight: FontWeight.w600, fontSize: 22, color: Colors.red),
        )),
        ...List.generate(
            _menuController.menuList.length,
            (index) => SlideMenuItemWidget(
                  _menuController.menuList[index],
                  _menuController.currentSelectIndex.value == index,
                  onTab: () {
                    _menuController.updateSelectIndex(index);
                  },
                )),
      ],
    );
  }
}

class SlideMenuItemWidget extends StatelessWidget {
  //显示的标题
  final String title;

  //是否选中
  final bool isSelect;

  //当前按钮点击事件
  final Function()? onTab;

  SlideMenuItemWidget(this.title, this.isSelect, {this.onTab});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: isSelect ? Colors.red : Colors.transparent,
      child: ListTile(
          onTap: onTab,
          //内边距
          contentPadding: const EdgeInsets.only(left: 20),
          //是否选中-不知道为啥，不起作用
          selected: isSelect,
          //选中的颜色-不知道为啥，不起作用
          selectedTileColor: Colors.red,
          title: Text(
            title,
            style: const TextStyle(
                fontWeight: FontWeight.w500, fontSize: 16, color: Colors.white),
          )),
    );
  }
}
