import 'dart:io';

import 'package:f_base/util/screen_utils.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:f_base/base_exp.dart';
import 'package:flutter_framework/app/pages/index_Page.dart';
import 'package:flutter_framework/app/routes/app_pages.dart';
import 'package:m_pc/web/pages/web_home_page.dart';
import 'package:m_pc_manager/pc_manager_page.dart';
import 'package:get/get_navigation/src/router_report.dart';

import 'app/navigator/my_bottom_app_bar.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // 解决flutter_screen release 包有些无法获取字体大小问题
  await ScreenUtil.ensureScreenSize();
  //设置状态栏
  App.submergence_status_bar();

  await App.initMain();
  //注册全局的getx controller，有些全局状态监控可以写到这个controller里面
  BaseController baseController = Get.put(BaseController());
  runApp(Obx(() {
    return ColorFiltered(
        colorFilter: ColorFilter.mode(baseController.isGrey.value == true ? Colors.white : Colors.transparent, BlendMode.color),
        child: GetMaterialApp(
            navigatorObservers: [GetXRouterObserver()],
            debugShowCheckedModeBanner: App.isDebug,
            defaultTransition: NavigatorUtil.transition,
            transitionDuration: NavigatorUtil.duration,
            title: "flutter 移动端 开发框架",
            theme: ThemeData(
              /// 配置应用程序的 亮色 暗色
              brightness: Brightness.light,
              /// 配置应用的主背景色
              primaryColor: GlobalStyle.themeColor,
              /// 配置应用程序前景色，如 一些系统组件的选中样式等
              colorScheme: ThemeData().colorScheme.copyWith(secondary: GlobalStyle.themeColor)
            ),
            translations: Messages(),
            locale: const Locale('zh', 'CH'),
            fallbackLocale: const Locale('en', 'US'), // 添加一个回调语言选项，以备上面指定的语言翻译不存在,
            //解决全局点击隐藏键盘问题
            builder: EasyLoading.init(builder: (context, child) {
              return Scaffold(
                //解决键盘弹起影响布局的问题
                resizeToAvoidBottomInset: false,
                body: GestureDetector(
                  onTap: () {
                    //点击屏幕隐藏弹出的键盘
                    ScreenUtils.hideKeyboard(context);
                  },
                  child: Container(
                    color: GlobalStyle.colorbg,
                    child: child,
                  ),
                ),
              );
            }),
            getPages: AppPages.pages,
            home: MyApp()
          // home: TabNavigator(),
        )
    );
  }
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    EasyLoading.init();
    if(kIsWeb || Platform.isMacOS || Platform.isWindows){
      return GlobalConfig.systemType == "1" ? PcManagerPage(): WebHomePage();
    } else {
      //不带底部+号的底部导航栏
//    return MyAppCommon.getApp(TabNavigator());
      //带底部＋号的底部导航栏
      //return MyAppCommon.getApp(widget: MyBottomAppBar());
      return IndexPage();
    }
  }
}

class GetXRouterObserver extends NavigatorObserver {
  @override
  void didPush(Route route, Route? previousRoute) {
    super.didPush(route, previousRoute);
    LogUtil.d('didPush: ${route.settings.name}');
    RouterReportManager.reportCurrentRoute(route);
  }

  @override
  void didPop(Route route, Route? previousRoute) {
    super.didPop(route, previousRoute);
    RouterReportManager.reportRouteDispose(route);
  }
}
