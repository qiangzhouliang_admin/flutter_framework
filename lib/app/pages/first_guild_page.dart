import 'package:f_base/base_exp.dart';
import 'package:flutter/material.dart';
import 'package:flutter_framework/app/pages/home_page.dart';

/// @Description 左右滑动引导页
/// @Author swan
/// @Date 2021/12/29 5:01 下午
///
class FirstGuildPage extends StatefulWidget {
  const FirstGuildPage({Key? key}) : super(key: key);

  @override
  _FirstGuildPageState createState() => _FirstGuildPageState();
}

class _FirstGuildPageState extends State<FirstGuildPage> {
  int? _currentIndex = 0;
  //要展示的图片
  final List<String> _imgPaths = [Res.sp01,Res.sp02,Res.sp03,Res.sp05,];

  @override
  Widget build(BuildContext context) {
    App.initApp(context);
    double width = CommonUtils.getScreenWidth(context);
    double height = CommonUtils.getScreenHeight(context);
    return Scaffold(
      backgroundColor: GlobalStyle.white,
      body: Stack(
        children: [
          //第一层的图片
          buildBackground(width, height),
          //第二层的点指示
          buildIndfot(),
          // 第三层的去首页
          buildGoHome()
        ],
      ),
    );
  }

  /// 指示点
  Positioned buildIndfot() {
    return Positioned(
      left: 0,
      right: 0,
      bottom: 60,
      height: 44.h,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ...List.generate(_imgPaths.length, (index) => buildIndefot(_currentIndex == index))
        ],
      ),
    );
  }

  Widget buildIndefot(bool isSelect) {
    return AnimatedContainer(
      height: 12.h,
      width: isSelect ? 40.w : 12.w,
      margin: EdgeInsets.only(left: 16.w),
      duration: const Duration(milliseconds: 800),
      decoration: BoxDecoration(
          color: Colors.deepOrangeAccent,
          borderRadius: BorderRadius.all(Radius.circular(10.h))),
    );
  }

  Positioned buildBackground(double width, double height) {
    return Positioned.fill(
      child: PageView(
      onPageChanged: ((value) {
        _currentIndex = value;
        setState(() {});
      }),
      children: [
        ...List.generate(_imgPaths.length, (index) => Image.asset(
          _imgPaths[index],
          width: width,
          height: height,
          fit: BoxFit.fill,
        ))
      ],
    ));
  }

  Widget buildGoHome() {
    return Positioned(
      left: 0,
      right: 0,
      bottom: 60,
      height: 44.h,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AnimatedContainer(
            duration: const Duration(milliseconds: 800),
            height: _currentIndex == _imgPaths.length-1 ? 44.h : 0,
            width: _currentIndex == _imgPaths.length-1 ? 160.w : 0,
            child: ElevatedButton(child: const Text("去首页"),onPressed: (){
              SpUtil.putInt(Constants.newFeature, (SpUtil.getInt(Constants.newFeature) ?? 0) + 1);
              NavigatorUtil.pushRightBack(HomePage(),isReplace: true);
            },),
          )
        ],
      ),
    );
  }
}
