import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


///气泡属性配置
class BobbleBean {
  //位置
  late Offset postion;

  //颜色
  late Color color;

  //运动的速度
  late double speed;

  //角度
  late double theta;

  //半径
  late double radius;
}
