import 'package:f_base/list_page/upload_refresh_list_widget.dart';
import 'package:flutter/material.dart';
import 'package:f_base/base_exp.dart';
import 'dart:math';

import 'goods_item_widget.dart';


/// @author 强周亮(qiangzhouliang)
/// @desc listview封装测试
/// @email 2538096489@qq.com
/// @time 2020/12/9 14:40
class ListViewTestPage extends StatefulWidget {
  @override
  _ListViewTestPageState createState() => _ListViewTestPageState();
}

class _ListViewTestPageState extends State<ListViewTestPage> {
  late UploadRefreshListWidget uploadRefreshListPage;
  List list = [];

  @override
  void initState() {
    super.initState();
    initData();
    //页面加载完成时，请求数据
    WidgetsBinding.instance.addPostFrameCallback((mag) {
      uploadRefreshListPage.getData();
    });
  }

  void initData() {
    Map<String,dynamic> params = {};
    params['clientType']="1";
    uploadRefreshListPage = UploadRefreshListWidget(
      context: context,
        params:params,
        onSuccess: (type,data){
          if(type == LoadType.LOAD){
            list.clear();
          }
          /*List<MyWaitListBean> listData = MyWaitFileModel.fromJson(data).data;
          if(listData != null && listData.isNotEmpty){
            listData.forEach((item) {
              list.add(itemWidget(name: item.gkxx??'',time: item.createTime??'',deptName: item.name??'',onTap: (){
                NavigatorUtil.pushRightBack(context, IndexListDetailPage(id: item.fLocalId));
              }));
            });
          } else {
            MyToast.toast(msg: '暂无数据');
          }*/
          for (var i = 0; i < 50; i++) {
            list.add(GoodsItemWidget(imgUrl: "https://dss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1723166067,628508292&fm=26&gp=0.jpg",));// 0,1,2,3,4,5,6,7,8,9
          }
          setState(() {});
        }, onError: (type,error){
      EasyLoading.showToast( '获取数据失败，请稍后重试');
    }, url: UrlContact.listTest,isAes: false);
  }

  SliverToBoxAdapter _topWidget() {
    return SliverToBoxAdapter(
      child: Container(
          height: 150.h,
          child: Column(
            children: <Widget>[
              Container(
                alignment: Alignment.bottomCenter,
                height: 150.h,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("images/icon_market_top.png"),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Container(
                  margin: EdgeInsets.all(3.h),
                  height: 50.h,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text('1111111111'),
                      Text('1111111111'),
                      Text('1111111111'),
                    ],
                  ),
                ),
              ),
            ],
          )),
    );
  }

  SliverPersistentHeader _floatWidget() {
    return SliverPersistentHeader(
      pinned: true, //是否固定在顶部
      floating: true,
      delegate: _SliverAppBarDelegate(
          minHeight: 50, //收起的高度
          maxHeight: 50, //展开的最大高度
          child: Container(
            padding: EdgeInsets.only(left: 16),
            color: Color(0xfff8f8f8),
            alignment: Alignment.centerLeft,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text("需求大厅"),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      "images/icon_filtrate.png",
                      width: 20.w,
                      height: 30.h,
                    ),
                    GestureDetector(
                      child: Padding(
                        padding: EdgeInsets.only(left: 5.w, right: 10.w),
                        child: Text(
                          "筛选",
                          style: TextStyle(
                              color: GlobalStyle.blueColor_deep,
                              fontSize: FontSizeStyle.tooBig),
                        ),
                      ),
                      onTap: () {

                      },
                    ),
                  ],
                )
              ],
            ),
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: myAppBar(context, title:"listView测试"),
      body: Column(
        children: [
          uploadRefreshListPage.listView(context,contentListData: list)
        ],
      ),
    );
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate({
    required this.minHeight,
    required this.maxHeight,
    required this.child,
  });

  final double minHeight;
  final double maxHeight;
  final Widget child;

  @override
  double get minExtent => minHeight;

  @override
  double get maxExtent => max(maxHeight, minHeight);

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return new SizedBox.expand(child: child);
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return maxHeight != oldDelegate.maxHeight ||
        minHeight != oldDelegate.minHeight ||
        child != oldDelegate.child;
  }
}
