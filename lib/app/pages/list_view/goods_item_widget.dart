import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:f_base/base_exp.dart';

class GoodsItemWidget extends StatelessWidget {
  final imgUrl;

  GoodsItemWidget({this.imgUrl});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left:4.0,top:4.0,bottom:6.0),
      color: Colors.white,
      child: Row(
        children: <Widget>[
          // 商品图片
          ExtendedImage.network(
            imgUrl,
            width: 100.w,
            height: 100.w,
            fit: BoxFit.fill,
            cache: true,
            //不设置shape时，下面的两个属性不起作用
            shape: BoxShape.rectangle,
            // border: Border.all(color: Colors.red, width: 1.0),
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            loadStateChanged: (ExtendedImageState state){
              if(state.extendedImageLoadState == LoadState.loading){
                return Container(
                  alignment: Alignment.center,
                  child: const Text("加载中。。。"),
                );
              } else if(state.extendedImageLoadState == LoadState.failed){
                return Image.asset(
                  Res.icon_no_data,
                  fit: BoxFit.fill,
                );
              } else {
                return null;
              }
            },
          ),
          Expanded(
            child: InkWell(
              onTap: (){
              },
              child: Container(
                padding: EdgeInsets.all(10.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      //上面部分
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            //标题
                            Text('925纯银款式可选耳钉7对',
                                style: TextStyle(
                                  fontSize: 14.sp,
                                    color: Colors.black),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1),
                            Text('奥通珠宝旗舰店',
                                style: TextStyle(
                                    color: Colors.grey),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1),
                            //券后价
                            Container(
                              margin: EdgeInsets.only(top: 8.0),
                              child: RichText(
                                text: TextSpan(children: <TextSpan>[
                                  TextSpan(
                                      text: "￥",
                                      style: TextStyle(
                                          color: Colors.pinkAccent,
                                          fontSize: 14.sp)),
                                  TextSpan(
                                      text: "10",
                                      style: TextStyle(
                                          color: Colors.pinkAccent,
                                          fontSize: 20.sp,
                                          fontWeight: FontWeight.w600)),
                                  TextSpan(
                                      text: "  券后价    ",
                                      style: TextStyle(
                                          color: Colors.pinkAccent,
                                          fontSize: 14.sp)),
                                  TextSpan(
                                      text: "原价 20",
                                      style: TextStyle(
                                          color: Colors.black38,
                                          decoration: TextDecoration.lineThrough,
                                          fontSize: 14.sp)),
                                ]),
                              ),
                            )
                          ],
                        ),
                      ),
                      //下面部分
                      Container(
                        child: Stack(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(left: 10),
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(255, 238, 230, 1.0),
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(100.0),
                                      bottomRight: Radius.circular(100.0))),
                              alignment: Alignment.centerLeft,
                              child: RichText(
                                text: TextSpan(children: [
                                  TextSpan(
                                    text: "已售",
                                    style: TextStyle(
                                        fontSize: 15.sp,
                                        color: Color.fromRGBO(137, 60, 17, 1.0)),
                                  ),
                                  TextSpan(
                                    text: " 3 ",
                                    style: TextStyle(
                                        fontSize: 20.sp,
                                        color: Color.fromRGBO(255, 91, 0, 1.0)),
                                  ),
                                  TextSpan(
                                    text: "件",
                                    style: TextStyle(
                                        fontSize: 14.sp,
                                        color: Color.fromRGBO(137, 60, 17, 1.0)),
                                  )
                                ]),
                              ),
                            ),
                            Positioned(
                                right: 0,
                                top: 0,
                                child: Container(
                                  width: 100.0,
                                  alignment: Alignment.center,
                                  child: MyButton.commonButton(context,title: '立即购买',height:22.5,mTop: 0.0,mBottom: 0.0,mLeft: 0.0,mRight: 0.0, onPress: (){

                                  }),
                                )
                            )
                          ],
                        ),
                      )
                    ]),
              ),
            )
          )
        ],
      ),
    );
  }
}
