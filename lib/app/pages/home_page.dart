import 'package:f_base/easyrefresh/basic.dart';
import 'package:f_base/util/log/log_util.dart';
import 'package:flutter/material.dart';
import 'package:f_base/base_exp.dart';
import 'package:flutter_framework/app/pages/TestTipsPage.dart';
import 'package:flutter_framework/app/pages/login/login_page.dart';
import 'package:flutter_framework/app/pages/travel_page/travel_page_page.dart';
import 'package:flutter_framework/app/widget/my_swiper.dart';

import '../routes/app_pages.dart';
import 'first_guild_page.dart';
import 'list_view/listview_test_page.dart';

//滚动的最大值,阈值
const APPBAR_SCROLL_OFFSET = 100;
//appbar透明度
double? appBarAlpha = 0;
/*首页界面*/
class HomePage extends StatefulWidget{
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with ProtocolModel {
  final List _imageUrls = [
    'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.zcool.cn%2Fcommunity%2F01d5025a2120e2a80120ba38d71b3d.jpg%402o.jpg&refer=http%3A%2F%2Fimg.zcool.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1642562496&t=322f02dd98bdfdc14b87598e0045fdc3',
    'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.zcool.cn%2Fcommunity%2F01e26d5a2120e2a801216e8dffa65b.jpg%402o.jpg&refer=http%3A%2F%2Fimg.zcool.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1642562496&t=6b2d6e1e099df7de0f58c486044b6c5e',
    'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fbpic.588ku.com%2Fback_pic%2F03%2F78%2F63%2F6057c18653a0319.jpg&refer=http%3A%2F%2Fbpic.588ku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1642562496&t=406d1d4c8db6d99e7e4bb5817ee56bd3'
  ];
  //样例列表
  List list = [];

  @override
  void initState() {
    super.initState();
    list..add(MySwiper(imageUrls: _imageUrls))..add(ListTile(
      title: const Text('点击跳转登录'),
      onTap: (){
        NavigatorUtil.pushRightBack(LoginPage());
      },
    ))..add(ListTile(
      title: const Text('动态权限获取'),
      onTap: (){
        PermissionUtil(perms: [Permission.storage,Permission.camera],success: (){
          EasyLoading.showToast("申请权限成功");
        }).requestPermission(PermissionTip.sdWRTip);
      },
    ))..add(ListTile(
      title: const Text('顶部标题栏滑动效果'),
      onTap: (){
        // NavigatorUtil.pushRightBack(TravelPage());
        // NavigatorUtil.pushRightBack(SWKeepAliveWrapper(child: TravelPagePage()));
        NavigatorUtil.toNamed(Routes.travelPagePage);
      },
    ))..add(ListTile(
      title: const Text('查看大图'),
      onTap: (){
        NavigatorUtil.pushRightBack(PhotoViewGalleryScreen(
          images: const [
            'https://img2.baidu.com/it/u=2468362699,2612376962&fm=253&app=138&size=w931&n=0&f=JPEG&fmt=auto?sec=1682010000&t=86ab7b6cbf260b1d2d9bac0ad99ee4fb',
            'https://img2.baidu.com/it/u=4093367817,2075246768&fm=253&app=120&size=w931&n=0&f=JPEG&fmt=auto?sec=1682010000&t=2bf8700451fd763e771deb4e07707da9',
          ],
          index: 0,
          heroTag: '美女',
        ));
      },
    ))..add(ListTile(
      title: const Text('下拉刷新，上拉加载更多'),
      onTap: (){
        NavigatorUtil.pushRightBack(const BasicPage('刷新加载'));
      },
    ))..add(ListTile(
      title: const Text('listView 封装'),
      onTap: (){
        NavigatorUtil.pushRightBack(ListViewTestPage());
      },
    ))..add(ListTile(
      title: const Text('检查更新'),
      onTap: (){
        CheckUpdateUtil().getAppVersion();
      },
    ))..add(ListTile(
      title: const Text('用户权限和隐私协议对话框'),
      onTap: () async {
        bool? isAgreement = await showProtocolFunction(context);
        if(isAgreement == true){
          LogUtil.e("同意协议");
        } else {
          LogUtil.e("不同意协议");
          //关闭应用
          CommonUtils.closeApp();
        }
      },
    ))..add(ListTile(
      title: const Text('左右滑动引导页面'),
      onTap: () {
        NavigatorUtil.pushRightBack(FirstGuildPage());
      },
    ))..add(ListTile(
      title: const Text('上下滚动的消息轮播'),
      onTap: () {
        NavigatorUtil.pushRightBack(TestTipsPage());
      },
    ));
  }

  @override
  Widget build(BuildContext context) {
    //初始化一些插件
    App.initApp(context);
    return Scaffold(
      //Stack 层叠组件，前面的元素在上面，后面的元素在下面
      body: Stack(
        children: <Widget>[
          // MediaQuery 具有删除padding的属性
          MediaQuery.removePadding(
            //移除顶部padding
              removeTop: true,
              context: context,
              //NotificationListener 可以监听列表的滚动
              child:NotificationListener(
                //监听列表的滚动
                onNotification: (scrollNotification){
                  //scrollNotification.depth 第0 个元素，也就是listview的第一个元素开始滚动的时候
                  if(scrollNotification is ScrollNotification && scrollNotification.depth == 0){
                    //滚动且是列表滚动的时候
                    _onScroll(scrollNotification.metrics.pixels);
                  }
                  return true;
                },
                child: ListView.builder(
                    itemCount: list.length,
                    itemBuilder: (context,index){
                      return list[index];
                    }
                ),
              )
          ),
          MyAppBar(appBarAlpha: appBarAlpha!),
        ],
      ),
    );
  }
  //滚动处理操作
  _onScroll(offset){
    double alpha = offset / APPBAR_SCROLL_OFFSET;
    if(alpha < 0){
      //向上滚动
      alpha = 0;
    }else if(alpha > 1){
      //向下滚动
      alpha = 1;
    }
    setState(() {
      appBarAlpha = alpha;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }
}