import 'package:f_base/base_exp.dart';
import 'package:flutter/material.dart';

/// @Description
/// @Author swan
/// @Date 2023/7/22 20:57
///

class TestTipsPage extends StatefulWidget {
  @override
  _TestTipsPageState createState() => _TestTipsPageState();
}

class _TestTipsPageState extends State<TestTipsPage> {
  List<String> loopList = [];
  @override
  void initState() {
    super.initState();
    for(int i = 0; i < 10; i++){
      loopList.add("测试数据$i");
    }
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Container(
          alignment: Alignment.center,
          width: double.infinity,
          height: 24,
          child: buildMarqueeWidget(loopList),
        ),
      ),
    );
  }

  MarqueeWidget buildMarqueeWidget(List<String> loopList) {
    ///上下轮播 安全提示
    return MarqueeWidget(
      //子Item构建器
      itemBuilder: (BuildContext context, int index) {
        String itemStr = loopList[index];
        //通常可以是一个 Text文本
        return Center(child: Text(itemStr));
      },
      //循环的提示消息数量
      count: loopList.length,
      loopSeconds: 1,
    );
  }

}
