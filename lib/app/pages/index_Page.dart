import 'package:f_base/base_exp.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_framework/app/navigator/my_bottom_app_bar.dart';
import 'package:flutter_framework/app/pages/first_guild_page.dart';

/// @Description 应用启动闪屏页
/// @Author swan
/// @Date 2021/12/30 9:37 上午
///
class IndexPage extends StatefulWidget {
  const IndexPage({Key? key}) : super(key: key);

  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> with ProtocolModel,CheckUpdateUtil{
  getSharedText() async {
    const String CHANNEL = "com.swan.shareData"; //这儿要与        MethodChannel(flutterView, CHANNEL)中CHANNEL名称一致
    const String invokeMethod = "shareData"; //这儿要与       call.method == invokeMethod中invokeMethod名称一致
    var channel = const MethodChannel(CHANNEL);
    var result = await channel.invokeMethod(invokeMethod);
    EasyLoading.showToast("message=>$result");
    print('message=>$result');
  }
  // 页面的初始化函数
  @override
  void initState() {
    super.initState();
    // 1 检查权限
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      // 2 用户权限和隐私政策
      initDataNext();
    });
  }

  @override
  Widget build(BuildContext context) {
    //初始化一些插件
    App.initApp(context);
    return Scaffold(
      body: Center(
        child: Image.asset(Res.welcome),
      ),
    );
  }

  /// 初始化工具类
  Future<void> initDataNext() async {
    await SpUtil.getInstance();
    //读取标识
    bool? isAgreement = SpUtil.getBool("isAgreement");
    if(isAgreement != true){
      isAgreement = await showProtocolFunction(context);
      // 保存标识
      SpUtil.putBool("isAgreement", isAgreement ?? false);
    }

    if(isAgreement == true){
      LogUtil.e("同意协议");
      //初始化登录信息
      UserManager.instance.init();
      next();//正式用next
      getSharedText();
    } else {
      LogUtil.e("不同意协议");
      //关闭应用
      CommonUtils.closeApp();
    }
  }

  Future<void> next() async {
    //检查更新
    // Todo: 开发时取消此注释，闪屏页添加检查更新功能
    //var result = await getAppVersion(isShowLoading: false,isShowToast: false);
    var result = false;
    // result:false 表示不更新或获取更新信息失败，直接进入下一步操作
    if(result == false){
      // 如果是第一次进入引导页面，否则 直接进入首页
      //读取标识
      int isFirstInto = SpUtil.getInt(Constants.newFeature) ?? 0;
      if(isFirstInto >= Constants.maxNewFeatureCount){
        // 进入首页
        //不带底部+号的底部导航栏
        //return MyAppCommon.getApp(TabNavigator());
        //带底部＋号的底部导航栏
        //return MyAppCommon.getApp(widget: MyBottomAppBar());
        NavigatorUtil.pushRightBack(MyBottomAppBar(),isReplace: true);
      } else {
        // 进入引导页
        NavigatorUtil.pushRightBack(FirstGuildPage(),isReplace: true);
      }
      //倒计时页面
    }
  }
}
