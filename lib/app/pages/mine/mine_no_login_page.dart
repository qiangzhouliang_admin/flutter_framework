import 'package:f_base/base_exp.dart';
import 'package:flutter/material.dart';
import 'package:flutter_framework/app/pages/login/login_page.dart';

/// @Description 用户未登录个人中心页
/// @Author swan
/// @Date 2021/12/30 2:48 下午
///
class MineNoLoginPage extends StatefulWidget {
  const MineNoLoginPage({Key? key}) : super(key: key);

  @override
  _MineNoLoginPageState createState() => _MineNoLoginPageState();
}

class _MineNoLoginPageState extends State<MineNoLoginPage> {
  bool isDown = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: const Color(0xffCDDEEC),
      child: Stack(
        alignment: Alignment.center,
        children: [
          GestureDetector(
            onTapDown: (TapDownDetails details) {
              // 手指按下时的回调
              setState(() {
                isDown = true;
              });
            },
            onTapCancel: () {
              //手指移出
              setState(() {
                isDown = false;
              });
            },
            onTap: () {
              //跳转登录页面
              setState(() {
                isDown = false;
              });
              NavigatorUtil.pushPageByFade(
                startMills: 1200,
                  context: context, targPage: LoginPage());
            },
            child: Hero(
              tag: "logo",
              child: Material(
                color: Colors.transparent,
                child: Container(
                  alignment: Alignment.center,
                  width: 76.w,
                  height: 76.w,
                  decoration: BoxDecoration(
                      color: Colors.redAccent,
                      borderRadius: BorderRadius.all(Radius.circular(43.w)),
                      boxShadow: isDown ? const [
                        BoxShadow(
                            offset: Offset(3, 4),
                            color: Colors.black,
                            blurRadius: 13)
                      ]: null),
                  child: Text(
                    "登录",
                    style: TextStyle(
                        fontSize: FontSizeStyle.big,
                        fontWeight: FontWeight.w500,
                        color: Colors.white),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
