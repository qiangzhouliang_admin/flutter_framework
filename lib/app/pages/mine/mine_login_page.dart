import 'package:f_base/base_exp.dart';
import 'package:flutter/material.dart';

/// @Description 用户已登录个人中心页
/// @Author swan
/// @Date 2021/12/30 2:48 下午
///
class MineLoginPage extends StatefulWidget {
  const MineLoginPage({Key? key}) : super(key: key);

  @override
  _MineLoginPageState createState() => _MineLoginPageState();
}

class _MineLoginPageState extends State<MineLoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            UserAccountsDrawerHeader(
              accountName: const Text('swan'),
              accountEmail: const Text('abc@itcast.cn'),
              currentAccountPicture: const CircleAvatar(
                backgroundImage: NetworkImage(
                    'https://images.gitee.com/uploads/91/465191_vsdeveloper.png?1530762316'),
              ),
              //美化当前控件
              decoration: const BoxDecoration(
                  //用户背景
                  image: const DecorationImage(
                      image: NetworkImage(
                          'http://www.liulongbin.top:3005/images/bg1.jpg'),
                      //背景填充模式
                      fit: BoxFit.cover)),
              //右上角
              otherAccountsPictures: [
                InkWell(
                  child: const Icon(
                    Icons.settings,
                    color: Colors.white,
                  ),
                  onTap: (){
                    EasyLoading.showToast("设置");
                  },
                )
              ],
            ),
            ListTitleU.cListTitle("退出登录", onTap: () {
              CommonDialog.show("您确定要退出登录吗?", doneClicked: () {
                UserManager.instance.clear();
              });
            })
          ],
        ),
      ),
    );
  }
}
