import 'package:f_base/base_exp.dart';
import 'package:f_model/model_exp.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_framework/app/pages/mine/mine_login_page.dart';

import 'mine_no_login_page.dart';

/// @desc 个人中心，我的
/// @author Swan
/// @email 2538096489@qq.com
/// @time 2021/12/30 2:32 下午
class MyPage extends StatefulWidget {
  @override
  _MyPageState createState() => _MyPageState();
}

class _MyPageState extends State<MyPage> {
  final LoginStreamController _loginStreamController = Get.put(LoginStreamController());
  @override
  Widget build(BuildContext context) {
    /*return AnnotatedRegion(
        child: Obx((){
          return buildMainBody();
        }),
        value: SystemUiOverlayStyle(
          statusBarBrightness: Brightness.light,
          statusBarColor: GlobalStyle.themeColor
        )
    );*/
    return Obx((){
      return buildMainBody();
    });
  }

  buildMainBody() {
    //判断用户是否已登录
    if (_loginStreamController.isLogin) {
      //如果用户已经登录，返回用户已登录的布局
      return const MineLoginPage();
    } else {
      // 如果用户未登录
      return const MineNoLoginPage();
    }
  }
}
