import 'package:f_base/app/my_app_bar.dart';
import 'package:f_base/style/global_style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TravelPageLogic extends GetxController with GetSingleTickerProviderStateMixin implements Bindings {
  late TabController tabCont;
  var carCount = 0;
  var count = 0.obs;

  @override
  void onInit() {
    super.onInit();
    tabCont = TabController(length: 3, vsync: this);
  }

  void addCarCount() {
    print('点击了增加车辆数量按钮');
    carCount++;
    update();
  }

  void addCount() {
    count++;
  }

  @override
  void onClose() {
    super.onClose();
    tabCont.dispose();
  }

  @override
  void dependencies() {
    Get.lazyPut<TravelPageLogic>(() => TravelPageLogic());
  }
}
/*旅拍*/
class TravelPagePage extends GetView<TravelPageLogic> {

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    // Get.put(TravelPageLogic());  //如果路由中没有配置的话，这儿需要 put 一下
    return GetBuilder<TravelPageLogic>(builder: (logic){
      //Scaffold 实现了基本的 Material Design 布局结构
      return Scaffold(
          appBar:myAppBar(context, title:'旅拍',tabBar: TabBar(
            labelColor: GlobalStyle.bluefontColor,
            unselectedLabelColor: Colors.grey,
            controller: controller.tabCont,
            tabs:const [
              Tab(icon:Icon(Icons.directions_car)),
              Tab(icon:Icon(Icons.directions_transit)),
              Tab(icon:Icon(Icons.directions_bike)),
            ],
          )),
          body:TabBarView(
            controller: controller.tabCont,
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('directions_car ${controller.carCount}'),
                  Obx(()=>Text('directions_car_count_Obx ${controller.count}')),
                  ObxValue((data)=>Text('directions_car_count_ObxValue ${data}'), controller.count),
                  ElevatedButton(
                      onPressed: controller.addCarCount,
                      child: Text("点击添加增加车辆数量")
                  ),
                  ElevatedButton(
                      onPressed: controller.addCount,
                      child: Text("点击添加增加计数")
                  )
                ],
              ),
              Text('directions_transit'),
              Text('directions_bike')
            ],
          )
      );
    });
  }
}
