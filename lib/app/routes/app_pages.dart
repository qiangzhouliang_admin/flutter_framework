import '../../app/modules/test_module/test_page.dart';
import '../../app/modules/test_module/test_bindings.dart';
import 'package:f_base/base_exp.dart';
import 'package:flutter_framework/app/pages/index_Page.dart';
import 'package:flutter_framework/app/pages/travel_page/travel_page_page.dart';

part './app_routes.dart';
/**
 * GetX Generator - fb.com/htngu.99
 * */

abstract class AppPages {
  static final pages = [
    GetPage(name: Routes.indexPage, page: () => IndexPage(),
      // middlewares: [ShopMiddleWare()]//中间件->作为页面跳转前的条件判断
    ),

    ///旅拍
    GetPage(
        name: Routes.travelPagePage,
        page: () => TravelPagePage(),//路由
        binding: TravelPageLogic() //全局的getxController,在这个地方写了后，不需要在使用的地方写 Get.put
    ),
    GetPage(
      name: Routes.TEST,
      page: () => testPage(),
      binding: testBinding(),
    ),
  ];
}
