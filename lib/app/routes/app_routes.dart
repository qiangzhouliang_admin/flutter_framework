part of './app_pages.dart';
/**
 * GetX Template Generator - fb.com/htngu.99
 * */

abstract class Routes {
  ///主页面
  static const String main = "/";
  static const String indexPage = "/indexPage";
  static const String travelPagePage = "/travelPagePage";
  static const TEST = '/test'; // test page
}
