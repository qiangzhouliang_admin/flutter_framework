import 'dart:io';

import 'package:f_base/update/app_update_controller.dart';
import 'package:f_base/util/screen_utils.dart';
import 'package:flutter/material.dart';
import 'package:f_base/base_exp.dart';
import 'package:ota_update/ota_update.dart';
import 'package:url_launcher/url_launcher.dart';

typedef APPUpdateDialogUpdateCallBack = void Function();

class APPUpdateDialog extends Dialog {
  String content;
  bool needForcedUpdating;
  String? androidDownloadUrl;
  String? iosDownloadUrl;
  String apkName;

  APPUpdateDialog(
      {Key? key,
      required this.content,
      required this.needForcedUpdating,
      this.androidDownloadUrl,
      this.iosDownloadUrl,
      this.apkName = "test.apk"})
      : super(key: key);

  /// app 更新controller
  final AppUpdateController _appUpdateController =
      Get.put(AppUpdateController());

  @override
  Widget build(BuildContext context) {
    double width = ScreenUtils.screenWidth - 80.w;
    return WillPopScope(
      child: Material(
        //创建透明层
        type: MaterialType.transparency, //透明类型
        child: Center(
          child: Container(
            decoration: BoxDecoration(
              color: Colors.transparent,
              border: Border.all(width: 0, color: Colors.transparent),
            ),
            width: width,
            padding: const EdgeInsets.all(0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                buildTitleImage(width),
                buildContent(width),
                BaseWidgetUtil.lineBase(),
                //下载进度条
                getBtnOrProgress(width)
              ],
            ),
          )),
      ),
      onWillPop: () async {
        return Future.value(false);
      },
    );
  }

  /// 中间内容
  Container buildContent(double width) {
    return Container(
      margin: const EdgeInsets.only(top: 0),
      width: width,
      padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(width: 0, color: Colors.white),
      ),
      child: Center(
        child: TextWidget.commonText(content: content,fontSize: FontSizeStyle.tooBig,fontWeight: FontWeight.w400,color: GlobalStyle.contentColor)
      ),
    );
  }

  Container buildTitleImage(double width) {
    return Container(
      width: width,
      height: width * 123.0 / 263.0,
      padding: const EdgeInsets.all(0),
      margin: const EdgeInsets.all(0),
      decoration: BoxDecoration(
        color: Colors.transparent,
        border: Border.all(width: 0.0, color: Colors.transparent),
      ),
      child: Image.asset(
        Res.app_update_titleImg,
        fit: BoxFit.fill,
      ),
    );
  }

  Container buildBtn(List<Widget> btnList) {
    return Container(
      decoration: const ShapeDecoration(
        color: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(14.0),
            bottomRight: Radius.circular(14.0),
          ),
        ),
      ),
      height: 55.h,
      width: ScreenUtils.screenWidth - 60.w,
      child: Row(
        children: btnList,
      ),
    );
  }

  /// 下载进度条样式
  Widget buildLineProgress(double width) {
    return Container(
      color: Colors.white,
      //限制进度条的高度
      height: 44.h,
      //限制进度条的宽度
      width: width,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          LinearProgressIndicator(
              //0~1的浮点数，用来表示进度多少;如果 value 为 null 或空，则显示一个动画，否则显示一个定值
              value: double.parse(_appUpdateController.progress) / 100,
              //背景颜色
              backgroundColor: const Color.fromRGBO(221, 221, 221, 1),
              //进度颜色
              valueColor:
                  const AlwaysStoppedAnimation<Color>(GlobalStyle.bluefontColor)),
          Container(
            margin: EdgeInsets.all(10.h),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Expanded(child: TextWidget.commonText(content: "下载中...",color: GlobalStyle.color999999)),
                TextWidget.commonText(content: "${_appUpdateController.progress}%",fontSize: FontSizeStyle.big,color: GlobalStyle.bluefontColor),
              ],
            ))
        ],
      ),
    );
  }

  Widget getBtnOrProgress(double width) {
    return Obx(() {
      if (_appUpdateController.isDownload) {
        return buildLineProgress(width);
      } else {
        List<Widget> btnList = <Widget>[
          Expanded(
            child: GestureDetector(
              onTap: () {
                if (Platform.isAndroid) {
                  //测试下载应用地址
                  if (androidDownloadUrl != null) {
                    _appUpdateController.isDownload = true;
                    downloadAndroidApp(androidDownloadUrl!);
                  } else {
                    EasyLoading.showToast( "没有发现下载地址");
                    Get.back(result: true);
                  }
                } else {
                  if (androidDownloadUrl != null) {
                    gotoAppStore(iosDownloadUrl!);
                  } else {
                    EasyLoading.showToast( "没有发现下载地址");
                    Get.back(result: true);
                  }
                }
              },
              child: TextWidget.commonText(content: '立即更新',textAlign: TextAlign.center,fontSize: FontSizeStyle.big,color: GlobalStyle.bluefontColor)
            ),
          ),
        ];

        if (!needForcedUpdating) {
          btnList.insert(0, BaseWidgetUtil.line_w(height: 44.h));
          btnList.insert(
              0,
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    Get.back(result: false);
                  },
                  child: TextWidget.commonText(content: '取消',textAlign: TextAlign.center,fontSize: FontSizeStyle.big,color: GlobalStyle.color999999),
                ),
              ));
        }
        return buildBtn(btnList);
      }
    });
  }

  downloadAndroidApp(String url) {
    try {
      OtaUpdate().execute(url, destinationFilename: apkName).listen(
        (OtaEvent event) {
          switch (event.status) {
            case OtaStatus.DOWNLOADING: // 下载中
              _appUpdateController.progress = event.value ?? "0";
              break;
            case OtaStatus.INSTALLING: //安装中
              _appUpdateController.isDownload = false;
              NavigatorUtil.pop();
              break;
            case OtaStatus.PERMISSION_NOT_GRANTED_ERROR: // 权限错误
              EasyLoading.showToast( "更新失败，请稍后再试 ");
              _appUpdateController.isDownload = false;
              NavigatorUtil.pop();
              break;
            default: // 其他问题
              break;
          }
        },
      );
    } catch (e) {
      EasyLoading.showToast( "更新失败，请稍后再试 ");
      _appUpdateController.isDownload = false;
      NavigatorUtil.pop();
    }
  }

  gotoAppStore(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw '不能跳转到当前地址： $url';
    }
  }
}
