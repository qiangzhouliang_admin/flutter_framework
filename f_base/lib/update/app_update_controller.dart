import 'package:f_base/base_exp.dart';
/// app 更新controller
class AppUpdateController extends GetxController{
  final RxBool _isDownload = false.obs;
  final RxString _progress = "0".obs;

  bool get isDownload => _isDownload.value;

  String get progress => _progress.value;

  set progress(String value) {
    _progress.value = value;
  }

  set isDownload(bool value) {
    _isDownload.value = value;
  }
}