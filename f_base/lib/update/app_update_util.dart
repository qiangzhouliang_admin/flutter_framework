import 'dart:io';
import 'package:f_model/model_exp.dart';
import 'package:f_base/base_exp.dart';
import 'package:package_info_plus/package_info_plus.dart';

/// @author 强周亮(qiangzhouliang)
/// @desc 检查更新工具类
/// @email 2538096489@qq.com
/// @time 2020/6/22 17:12

mixin class CheckUpdateUtil {
  /// 接口获取当前最新版本信息
  /// 返回值false 表示不更新，进行下一步操作
  Future<Object> getAppVersion({isShowToast = true,isShowLoading = true}) async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    //当前版本号
    String appVersion = packageInfo.version;
    /*var data = await HttpUtil(isShowLoading: isShowLoading).get(
        {"clientType": Platform.isAndroid ? '1' : '2'},
        loadUrl: UrlContact.getUpdate);
    if (data == null) {
      if (isShowToast) {
        ToastUtils.showToast( '获取版本信息失败，请稍后重试');
      }
      return false;
    }*/
    ResponseInfo responseInfo = await DioUtils.instance.getRequest(
      isShowLoading:isShowLoading,loadingContent:"正在获取版本信息，请稍后...",
        url: UrlContact.getUpdate,queryParameters:{"clientType": Platform.isAndroid ? '1' : '2'},isAes: false);
    if(responseInfo.success){
      var data = responseInfo.data;
      AppUpdateEntity entity = AppUpdateEntity.fromJson(data);
      UserManager().setAppUpdateEntity(entity);
      UserManager().setAppVersion(appVersion);
      String? lastVersion = entity.version;
      if (lastVersion != null && appVersion.compareTo(lastVersion) != 0) {
        //有新版本
        return await Get.dialog(
            APPUpdateDialog(
              content: entity.description ?? "",
              needForcedUpdating: entity.isForceUpdate?.compareTo('true') == 0,
              androidDownloadUrl: UrlContact.download,
            ),
            barrierDismissible: false);
      } else {
        if (isShowToast) {
          EasyLoading.showToast('当前已经是最新版本，无需更新');
        }
        return false;
      }
    } else {
      if (isShowToast) {
        EasyLoading.showToast( '获取版本信息失败，请稍后重试');
      }
      return false;
    }

  }

  //检查更新
  void checkUpdate() {
    getAppVersion();
  }
}
