import 'package:dio/dio.dart';
import 'package:flutter/widgets.dart';

/// @author swan
/// @desc dio 网络请求拦截日志
/// @email 2538096489@qq.com
/// @time 2021/12/30 5:03 下午
class LogsInterceptors extends InterceptorsWrapper {

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    debugPrint("\n================== 请求数据 ==========================");
    var url = options.baseUrl + options.path;
    if(options.path.startsWith("https://") || options.path.startsWith("http://")){
      url = options.path;
    }
    debugPrint("|请求url：$url");
    debugPrint('|请求头: ${options.headers}');
    debugPrint('|请求参数: ${options.queryParameters}');
    debugPrint('|请求方法: ${options.method}');
    debugPrint("|contentType = ${options.contentType}");
    debugPrint('|请求时间: ${DateTime.now()}');
    if (options.data != null) {
      debugPrint('|请求数据: ${options.data}');
    }


    return super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    debugPrint("\n|================== 响应数据 ==========================");
    debugPrint("|url = ${response.realUri}");
    debugPrint("|statusCode = ${response.statusCode}");
    debugPrint("|data = ${response.data}");
    debugPrint('|返回时间: ${DateTime.now()}');
    debugPrint("\n");

    return super.onResponse(response, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    debugPrint("\n================== 错误响应数据 ======================");
    //print("|url = ${e.request.path}");
    debugPrint("|type = ${err.type}");
    debugPrint("|message = ${err.message}");

    debugPrint('|response = ${err.response}');
    debugPrint("\n");

    return super.onError(err, handler);
  }
}
