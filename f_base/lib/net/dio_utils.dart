import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:f_base/app/app.dart';
import 'package:f_base/dialog/loadingDialog.dart';
import 'package:f_base/net/address/api.dart';
import 'package:f_base/util/aes_util.dart';

import 'dio_common.dart';
import 'log_interceptor.dart';
import 'response_info.dart';


/// @author swan
/// @desc 网络请求
/// @email 2538096489@qq.com
/// @time 2021/12/30 5:02 下午
class DioUtils extends DioCommon{
  late Dio _dio;

  // 工厂模式
  factory DioUtils() => _getInstance();

  static DioUtils get instance => _getInstance();
  static DioUtils? _instance;

  //配置代理标识 false 不配置
  bool isProxy = false;



  DioUtils._internal() {
    BaseOptions options = BaseOptions();
    //请求时间
    options.baseUrl = API.BaseUrl;
    options.connectTimeout = const Duration(seconds: 20000);
    options.receiveTimeout = const Duration(seconds: 2 * 60 * 1000);
    options.sendTimeout = const Duration(seconds: 2 * 60 * 1000);
    // 初始化
    _dio = Dio(options);
    if (App.isDebug) {
      debugFunction();
    }
  }

  static DioUtils _getInstance() {
    _instance ??= DioUtils._internal();
    return _instance!;
  }

  void debugFunction() {
    // 添加log
    _dio.interceptors.add(LogsInterceptors());
    //配置代理
    if (isProxy) {
      DioCommon.setupPROXY(_dio);
    }
  }



  /// get 请求
  ///[url]请求链接
  ///[queryParameters]请求参数
  ///[cancelTag] 取消网络请求的标识
  Future<ResponseInfo> getRequest({
    required String url,
    Map<String, dynamic>? queryParameters,
    CancelToken? cancelTag,
    bool isShowLoading = true,
    String loadingContent = "数据加载中，请稍后...",
    bool isAes = true
  }) async {
    //发起get请求
    try {
      _dio.options = await buildOptions(_dio.options);
      //设置请求头
      _dio.options.headers.addAll(setHeadMethod(url,isShowLoading:isShowLoading,loadingContent: loadingContent));
      //_dio.options.headers["content-type"] = "application/json";
      //判断参数是否加密，如果需要加密，先加密参数
      if(queryParameters != null && isAes && !exceptAes(url)){
        queryParameters = AesEncryptParam(queryParameters);
      }
      Response response = await _dio.get(url,
          queryParameters: queryParameters, cancelToken: cancelTag);
      //响应数据
      return resultOperate(response);
    } catch (e, s) {
      //异常
      //关闭请求加载进度框
      closeShowLoading();
      return errorController(e, s);
    }
  }

  /// post 请求
  ///[url]请求链接
  ///[formDataMap]formData 请求参数
  ///[jsonMap] JSON 格式
  Future<ResponseInfo> postRequest({
    required String url,
    Map<String, dynamic>? formDataMap,
    Map<String, dynamic>? jsonMap,
    CancelToken? cancelTag,
    bool isShowLoading = true,
    String loadingContent = "数据加载中，请稍后...",
    bool isAes = true
  }) async {
    _dio.options = await buildOptions(_dio.options);
    // _dio.options.headers["content-type"]="multipart/form-data";
    //设置请求头
    _dio.options.headers.addAll(setHeadMethod(url,isShowLoading:isShowLoading,loadingContent: loadingContent));
    dynamic data = formDataMap == null ? jsonMap : FormData.fromMap(formDataMap);
    if((formDataMap != null || jsonMap != null) && isAes && !exceptAes(url)) {
      //参数加密
      if (formDataMap == null) {
        data = AesUtil.aesEncrypt(jsonEncode(jsonMap));
      } else {
        data = FormData.fromMap(AesEncryptParam(formDataMap));
      }
    }
    //发起post请求
    try {
      Response response = await _dio.post(url, data: data, cancelToken: cancelTag);
      //响应数据
      return resultOperate(response);
    } catch (e, s) {
      //关闭请求加载进度框
      closeShowLoading();
      return errorController(e, s);
    }
  }
}

