class API {
  // 接口文档
  //https://console-docs.apipost.cn/preview/f6b7ea7903a68cb0/717dddb7cf46ba9c
  // 调试接口地址
  // https://mock.yonyoucloud.com/project/23228/interface/api/100463

  //测试地址
  // static const String BaseUrl = 'https://mock.yonyoucloud.com/mock/23228';
  static const String BaseUrl = 'https://console-mock.apipost.cn/mock/7d88f73d-5097-499c-ad5f-2f72125bb4e3';
  static const String basicApi = "$BaseUrl/flutterTest/";

  //配置不加密的URL
  static var passUrl = ["flutterTest/list"];
  //配置不需要传token的地址
  static var exceptTokenUrl = ["flutterTest/list"];
}
