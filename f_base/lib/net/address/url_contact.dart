
import 'api.dart';

/// @author 强周亮(qiangzhouliang)
/// @desc 请求接口地址统一配置
/// @email 2538096489@qq.com
/// @time 2020/8/11 18:22
class UrlContact {

  // 接口文档
  //https://console-docs.apipost.cn/preview/f6b7ea7903a68cb0/717dddb7cf46ba9c
  // list 列表测试
  static const String listTest = "/api/demo/list";

  ///========================APP信息----开始========================
  //获取APP更新信息
  static const String getUpdate = "${API.BaseUrl}/apkVersionCheck";
  //app 下载地址
  static const String download = "http://202.100.92.55:8089/hzz/apk/download?isDownload=00A";
  //iOS下载地址
  static const String iosDownload = 'http://www.baidu.com';
///========================APP信息----结束========================

}