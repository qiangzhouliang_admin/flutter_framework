/// @Description 请求结果统一处理类
/// @Author swan
/// @Date 2021/12/31 9:59 上午
/// 
class ResponseInfo {
  bool success;
  int code;
  String message;
  dynamic data;

  ResponseInfo(
      {this.success = true, this.code = 200, this.data, this.message = "请求成功"});

  ResponseInfo.error(
      {this.success = false, this.code = 201, this.message = "请求异常"});
}