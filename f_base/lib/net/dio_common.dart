import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:dio/io.dart';
import 'package:f_base/app/user_manager.dart';
import 'package:f_base/dialog/loadingDialog.dart';
import 'package:f_base/net/address/api.dart';
import 'package:package_info_plus/package_info_plus.dart';

import 'response_info.dart';

/// @Description dio 请求的一些公共操作
/// @Author swan
/// @Date 2021/12/31 9:55 上午
/// 
class DioCommon {
  /// 配置代理
  static void setupPROXY(Dio dio) {
    //网络代理地址
    String proxyIp = "192.168.0.107";

    //网络代理端口
    String proxyPort = "8888";
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.findProxy = (uri) {
        //proxyIp 地址  proxyPort 端口
        return 'PROXY $proxyIp : $proxyPort';
      };
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) {
        //忽略证书
        return true;
      };
    };
  }

  /// 对请求回来的结果进行统一处理
  ResponseInfo resultOperate(Response<dynamic> response) {
    dynamic responseData = response.data;
    //关闭请求加载进度框
    closeShowLoading();
    if (responseData is Map<String, dynamic>) {
      Map<String, dynamic> responseMap = responseData;
      return buildResult(responseMap);
    } else if(jsonDecode(responseData) is Map<String, dynamic>){
      Map<String, dynamic> responseMap = jsonDecode(responseData);
      return buildResult(responseMap);
    } else {
      return ResponseInfo.error(code: 503, message: "数据格式无法识别");
    }
  }

  ResponseInfo buildResult(Map<String, dynamic> responseMap) {
    int? code = responseMap["code"];
    if (responseMap != null || code == 200) {
      //业务代码处理正常
      //获取数据
      dynamic data = responseMap["data"];
      return ResponseInfo(data: data);
    } else {
      //业务代码异常
      return ResponseInfo.error(
          code: responseMap["code"], message: responseMap["message"]);
    }
  }

  /// 不需要加密的地址
  bool exceptAes(String url){
    bool result = false;
    for (var element in API.passUrl) {
      if(url.contains(element)){
        result = true;
        break;
      }
    }
    return result;
  }


  /// 错误结果统一处理
  Future<ResponseInfo> errorController(e, StackTrace s) {
    ResponseInfo responseInfo = ResponseInfo();
    responseInfo.success = false;

    //网络处理错误
    if (e is DioError) {
      DioError dioError = e;
      switch (dioError.type) {
        case DioExceptionType.connectionTimeout:
          responseInfo.message = "连接超时";
          break;
        case DioExceptionType.sendTimeout:
          responseInfo.message = "请求超时";
          break;
        case DioExceptionType.receiveTimeout:
          responseInfo.message = "响应超时";
          break;
        case DioExceptionType.badResponse:
        // 响应错误
          responseInfo.message = "响应错误";
          break;
        case DioExceptionType.cancel:
        // 取消操作
          responseInfo.message = "已取消";
          break;
        case DioExceptionType.unknown:
        // 默认自定义其他异常
          responseInfo.message = "网络请求异常";
          break;
        case DioExceptionType.badCertificate:
          responseInfo.message = "配置证书不正确";
          break;
        case DioExceptionType.connectionError:
          responseInfo.message = "连接错误";
          break;
      }
    } else {
      //其他错误
      responseInfo.message = "未知错误";
    }
    responseInfo.success = false;
    return Future.value(responseInfo);
  }
  /// 检查不需要传 token的地址
  bool exceptTokenUrl(String url) {
    bool result = false;
    for (var element in API.exceptTokenUrl) {
      if(url.contains(element)){
        result = true;
        break;
      }
    }
    return result;
  }

  Future<BaseOptions> buildOptions(BaseOptions options) async {
    ///请求header的配置
    options.headers["productId"] = Platform.isAndroid ? "Android" : "IOS";
    options.headers["application"] = "coalx";

    //获取当前App的版本信息
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String appName = packageInfo.appName;
    String packageName = packageInfo.packageName;
    String version = packageInfo.version;
    String buildNumber = packageInfo.buildNumber;
    options.headers["appVersion"] = "$version";

    return Future.value(options);
  }

  /// @desc 设置请求头
  /// @author 强周亮
  /// @time 2020/8/27 15:59
  Map<String, String> setHeadMethod(String loadUrl,{
    bool isShowLoading = true,
    String loadingContent = "数据加载中...",
    contentType = "application/json;charset=UTF-8"}) {
    if(isShowLoading) {
      showLoading(content: loadingContent);
    }
    Map<String, String> headersMap = {};
    headersMap["content-type"] = contentType;
    if(loadUrl.contains("/token")) {
      headersMap["content-type"] = "application/x-www-form-urlencoded";
    } else if(!exceptTokenUrl(loadUrl)){
      headersMap["Authorization"] = UserManager().tokenModel?.accessToken??'';
    }
    return headersMap;
  }
}