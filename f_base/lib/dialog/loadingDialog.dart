import 'package:flutter/material.dart';
import 'package:f_base/base_exp.dart';

// ignore: must_be_immutable
class LoadingDialog extends Dialog {
  String text;

  LoadingDialog({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material( //创建透明层
      type: MaterialType.transparency, //透明类型
      child: Center( //保证控件居中效果
        child: SizedBox(
          child: Container(
            padding: const EdgeInsets.only(left: 14,right: 20,top: 10,bottom: 14),
            decoration: const ShapeDecoration(
              color: Color(0x99000000),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(8.0),
                ),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const SizedBox(
                  width: 20,
                  height: 20,
                  child: CircularProgressIndicator(strokeWidth: 2,),),
                Container(
                  margin: EdgeInsets.only(left: 16.w),
                  child: Text(text, style: TextStyle(fontSize: 12.sp,color: GlobalStyle.white),),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}


/// @desc 显示正在加载中
/// @author 强周亮
/// @time 2020/8/12 11:42
void showLoading({content='正在加载中...'}) {
  if(Get.isDialogOpen == false){
    Get.dialog(
      LoadingDialog(text: content,),
      barrierDismissible: false
    );
  }
}

/// @desc 关闭正在加载中
/// @author 强周亮
/// @time 2020/8/12 11:42
void closeShowLoading() {
  if(Get.isDialogOpen == true){
    NavigatorUtil.pop();
  }
}