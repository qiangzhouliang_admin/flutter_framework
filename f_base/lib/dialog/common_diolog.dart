import 'package:flutter/material.dart';
import 'package:f_base/base_exp.dart';

typedef void DoneClicked();

/// @author swan
/// @desc 公共dialog
/// @email 2538096489@qq.com
/// @time 2020/6/23 18:29
class CommonDialog extends Dialog {
  String content;
  String title;
  String negative;
  String positive;
  bool isShowCancle;
  DoneClicked? doneClicked;

  static void show(content,
      {barrierDismissible = false,
      isShowCancle = true,
      doneClicked,
      title = '温馨提示',
      positive = '确定',
      negative = '取消'}) {
    Get.dialog(
        CommonDialog(
          content: content,
          isShowCancle: isShowCancle,
          title: title,
          negative: negative,
          positive: positive,
          doneClicked: () {
            if (Get.isDialogOpen == true) {
              NavigatorUtil.pop();
            }
            doneClicked();
          },
        ),
        barrierDismissible: barrierDismissible);
  }

  CommonDialog(
      {Key? key,
      required this.content,
      this.isShowCancle = true,
      this.doneClicked,
      required this.title,
      this.negative = "取消",
      this.positive = "确定"})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double rasius = 10.w;

    Widget titleImgWidget = Container(
      height: 44.w,
      decoration: BoxDecoration(
          color: GlobalStyle.themeColor.withOpacity(0.6),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(rasius),
            topRight: Radius.circular(rasius),
          )),
      child: Center(
        child: Text(
          title,
          style: TextStyle(color: GlobalStyle.white, fontSize: FontSizeStyle.big),
        ),
      ),
    );

    Widget contentWidget = Container(
      padding: EdgeInsets.only(left: 20.w, right: 20.w, bottom: 10.h, top: 10.h),
      constraints: BoxConstraints(minHeight: 100.w,),
      decoration: BoxDecoration(
        color: GlobalStyle.white,
      ),
      child: Center(
        child: Text(
          content,
          style: (TextStyle(
            fontSize: FontSizeStyle.tooBig,
            fontWeight: FontWeight.w400,
            color: GlobalStyle.contentColor,
          )),
        ),
      ),
    );

    List<Widget> btnList = <Widget>[
      Expanded(
        child: GestureDetector(
          onTap: () {
            if (doneClicked != null) {
              doneClicked!();
            }
          },
          child: Text(
            positive,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: FontSizeStyle.big, color: GlobalStyle.bluefontColor),
          ),
        ),
      ),
    ];

    if (isShowCancle) {
      btnList.insert(0, BaseWidgetUtil.line_w(height: 44.h));

      btnList.insert(
          0,
          Expanded(
            child: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Text(
                negative,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: FontSizeStyle.big,
                    color: const Color.fromRGBO(153, 153, 153, 1)),
              ),
            ),
          ));
    }

    Widget reviewBtnWidget = Container(
      decoration: ShapeDecoration(
        color: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(rasius),
            bottomRight: Radius.circular(rasius),
          ),
        ),
      ),
      height: 54.h,
      child: Row(
        children: btnList,
      ),
    );

    return WillPopScope(
      child: Material(
        //创建透明层
        type: MaterialType.transparency, //透明类型
        child: Center(
          child: Container(
            margin: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: Colors.transparent,
              border: Border.all(width: 0, color: Colors.transparent),
            ),
            padding: EdgeInsets.only(left: 20.w, right: 20.w),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                titleImgWidget,
                contentWidget,
                BaseWidgetUtil.lineBase(),
                reviewBtnWidget,
              ],
            ),
        )),
      ),
      onWillPop: () async {
        return Future.value(false);
      },
    );
  }
}
