import 'package:flutter/material.dart';
import 'package:easy_refresh/easy_refresh.dart';

ClassicFooter footer() {
  return ClassicFooter(
    backgroundColor: Colors.grey[200]!,
    dragText: "上拉加载更多",
    armedText: '释放加载',
    readyText: '正在加载...',
    processingText: '正在加载...',
    processedText: '加载完成',
    failedText: '加载失败',
    noMoreText: '没有更多数据',
    messageText: '更新于 %T',
  );
}