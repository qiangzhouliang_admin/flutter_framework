/// @Description 申请权限提示内容
/// @Author swan
/// @Date 2021/12/28 3:26 下午
/// 
class PermissionTip {
  //内存卡读写权限
  static List<String> sdWRTip = [
    "为您更好的体验应用，所以需要获取您手机文件存储权限，以保存您的一些偏好设置",
    "您已拒绝权限，所以无法保存您的一些偏好设置，将无法使用app",
    "您已拒绝权限，请在设置中心中同意APP的权限请求",
    "其他错误"
  ];
}