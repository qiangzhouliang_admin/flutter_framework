import 'package:f_base/base_exp.dart';
import 'package:flutter/material.dart';

/// @author 强周亮(qiangzhouliang)
/// @desc 动态权限申请工具类
/// @email 2538096489@qq.com
/// @time 2020/6/23 18:23

class PermissionUtil with WidgetsBindingObserver{
  final Success success;
  // 是否去设置中心
  bool _isGoSetting = false;
  List<Permission> perms;

  PermissionUtil({required this.perms,required this.success});
  Future requestPermission(List<String> tipContent) async {
    //注册观察者
    WidgetsBinding.instance.addObserver(this);

    // 申请权限[Permission.storage,Permission.camera]
    //判断是否已经申请了权限
    var haveTotalGranted = 0;
    for (var element in perms) {
      if(await isHavaPermission(element)){
        haveTotalGranted ++;
      }
    }
    //表示已经有权限，直接返回成功信息
    if(haveTotalGranted >= perms.length) {
      //权限申请成功
      //注销观察者
      WidgetsBinding.instance.removeObserver(this);
      success();
    } else {
      CommonDialog.show(tipContent[0],doneClicked: () async {
        //有可能有一部分没有权限，申请权限
        Map? map = await _requestPerm(perms);
        map?.forEach((key,value) async {
          if(value == PermissionStatus.denied){
            //用户第一次申请拒绝
            CommonDialog.show(tipContent[1],doneClicked: () async {
              if(Get.isDialogOpen == true){
                NavigatorUtil.pop();
              }
              //申请权限
              _requestPerm(perms);
            },positive: "重试");
          }else if (value == PermissionStatus.permanentlyDenied) {
            //第二次申请权限用户拒绝
            CommonDialog.show(tipContent[2],doneClicked: () async {
              if(Get.isDialogOpen == true){
                NavigatorUtil.pop();
              }
              _isGoSetting = true;
              await openAppSettings();
            },positive: "去设置中心");
          }
        });
      },positive: "同意");
    }

  }


  //监听app生命周期的变化来处理打开设置中心操作之后的回调检测
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if(state == AppLifecycleState.resumed && _isGoSetting){
      _requestPerm(perms);
    }
  } //判断是否有这个权限
  static Future<bool> isHavaPermission(Permission permission) async {
    return await permission.isGranted;
  }

  //第二次申请权限
  Future<Map?> _requestPerm(List<Permission> perms) async {
    Map<Permission, PermissionStatus> statuses = await perms.request();
    var totalGranted = 0;
    //返回是否所有权限已申请的结果
    bool permResult = false;
    statuses.forEach((key, value) async {
      if (value.isGranted) {
        totalGranted++;
        if (totalGranted >= statuses.length) {
          permResult = true;
        }
      }
    });
    if (permResult) {
      //注销观察者
      WidgetsBinding.instance.removeObserver(this);
      success();
      return null;
    } else {
      return statuses;
    }
  }
}
//注意 typedef 需要放在class外面
//权限申请成功
typedef Success = void Function();
