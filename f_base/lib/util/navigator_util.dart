import 'package:flutter/material.dart';
import 'package:get/get.dart';


class NavigatorUtil {
  static const Transition transition = Transition.cupertino;
  static const Duration duration = Duration(milliseconds: 850);

  ///跳转到指定页面
  static push(BuildContext context, Widget page) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => page));
  }
  ///导航到新的页面。
  /// isReplace : 是否关闭前一个页面
  static pushRightBack(Widget page,{bool isReplace = false}) {
    if(isReplace){
      //进入下一个页面，关闭前一个页面
      return Get.off(page,transition: transition,duration: duration);
    } else {
      //进入下一个页面
      return Get.to(page, transition: transition, duration: duration);
    }
  }
  ///普通打开页面的方法
  ///[context] 上下文对象
  ///[targPage] 目标页面
  ///[isReplace] 是否替换当前页面  A -B
  ///[opaque] 是否以背景透明的方式打开页面
  static void pushPageByFade({
    required BuildContext context,
    required Widget targPage,
    bool isReplace = false,
    int startMills = 400,
    bool opaque = false,
    Function(dynamic value)? dismissCallBack,
  }) {
    PageRoute pageRoute = PageRouteBuilder(
      //背景透明 方式打开新的页面
      opaque: opaque,
      pageBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondaryAnimation) {
        return targPage;
      },
      transitionDuration: Duration(milliseconds: startMills),
      //动画
      transitionsBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondaryAnimation, Widget child) {
        return FadeTransition(
          opacity: animation,
          child: child,
        );
      },
    );

    if (isReplace) {
      Navigator.of(context).pushReplacement(pageRoute).then((value) {
        if (dismissCallBack != null) {
          dismissCallBack(value);
        }
      });
    } else {
      Navigator.of(context).push(pageRoute).then((value) {
        if (dismissCallBack != null) {
          dismissCallBack(value);
        }
      });
    }
  }
  ///返回上一页面
  ///param 要传回上一个页面的参数
  static pop({param}){
    return Get.back(result: param);
  }

  ///进入下一个页面，但没有返回上一个页面的选项（用于SplashScreens，登录页面等）。
  static off(Widget NextScreen){
    return Get.off(NextScreen,transition: transition,duration: duration);
  }

  /// @desc 进入下一个界面并取消之前的所有路由（在购物车、投票和测试中很有用）
  /// @author 强周亮
  /// @time 2020/8/17 16:10
  static pushAndRemoveUntilCloseAll(Widget NextScreen) {
    Get.offAll(NextScreen,transition: transition,duration: duration);
  }

  /// ========================路由导航=================================
  /// 路由导航需要先声明路由
  ///void main() {
  //   runApp(
  //     GetMaterialApp(
  //       unknownRoute: GetPage(name: '/notfound', page: () => UnknownRoutePage()),
  //       initialRoute: '/',
  //       getPages: [
  //         GetPage(name: '/', page: () => MyHomePage()),
  //         GetPage(name: '/second', page: () => Second()),
  //         GetPage(
  //           name: '/third',
  //           page: () => Third(),
  //           transition: Transition.zoom
  //         ),
  //       ],
  //     )
  //   );
  // }

  /// 路由导航 导航到下一个页面
  /// Get在这里接受任何东西，无论是一个字符串，一个Map，一个List，甚至一个类的实例。
  /// 在页面中接收：在你的类或控制器上：print(Get.arguments);
  static toNamed(String router,{arguments}){
    Get.toNamed(router,arguments: arguments);
  }

  /// 浏览并删除前一个页面。
  static offNamed(String router,{arguments}){
    Get.offNamed(router,arguments: arguments);
  }

  /// 浏览并删除所有以前的页面。
  static offAllNamed(String router,{arguments}){
    Get.offAllNamed(router,arguments: arguments);
  }
}
