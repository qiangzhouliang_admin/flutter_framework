import 'package:encrypt/encrypt.dart';


/// @author 强周亮(qiangzhouliang)
/// @desc aes 加密工具
/// @email 2538096489@qq.com
/// @time 2020/7/31 10:14
class AesUtil{
  static Encrypter? encrypter;
  static IV? iv;
  static initAes({keys = 'ec19479f79b2a225',mode = AESMode.ecb, padding = 'PKCS7'}){
    Key key = Key.fromUtf8(keys);
    iv = IV.fromLength(16);
    encrypter = Encrypter(AES(key, mode: AESMode.cbc,padding: padding));
  }

  //aes加密
  static String aesEncrypt(String content) {
    try {
      if(encrypter == null){
        initAes();
      }
      final encrypted = encrypter?.encrypt(content,iv: iv);
      return encrypted?.base64 ?? "";
    } catch (err) {
      print("aes encode error:$err");
      return content;
    }
  }

  //aes解密
  static dynamic aesDecrypt(dynamic base64) {
    try {
      if(encrypter == null){
        initAes();
      }
      return encrypter?.decrypt64(base64,iv: iv);
    } catch (err) {
      print("aes decode error:$err");
      return base64;
    }
  }
}

/// @desc 参数加密解密方法
/// @author 强周亮
/// @time 2020/7/31 10:10
/// isEncOrDec true 表示加密 false 表示解密,默认加密
Map<String, dynamic> AesEncryptParam(Map<String, dynamic> paraMap,{isEncOrDec = true}) {
  Map<String, dynamic> resMap = {};
  paraMap.forEach((key, value) {
    if(value is String){
      if (isEncOrDec) {
        resMap[key] = AesUtil.aesEncrypt(value);
      } else {
        resMap[key] = AesUtil.aesDecrypt(value);
      }
    } else if(value is Map<String,dynamic>){
      resMap[key] = AesEncryptParam(value,isEncOrDec: isEncOrDec);
    } else if(value is List){
      var list = [];
      for (var element in value) {
        list.add(isEncOrDec ? AesUtil.aesEncrypt(element) : AesUtil.aesDecrypt(element));
      }
      resMap[key] = list;
    }
  });
  return resMap;
}

void main(){
  var encrypted = AesUtil.aesEncrypt("ADMIN");
  print(encrypted);

//  var content = aes.decrypt(encrypted);
  var content = AesUtil.aesDecrypt(encrypted);
  print(content);

  var testMap = {
    "test1":"test1",
    "testObj":{
      "testObj1":"testObj1",
      "testObj2":"testObj2",
    },
    "testList":["1","2","3"]
  };
  var ressMap = AesEncryptParam(testMap);
  var docMap = AesEncryptParam(ressMap,isEncOrDec: false);
  print(testMap);
  print(ressMap);
  print(docMap);
}
