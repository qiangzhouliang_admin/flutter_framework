import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

/// @author swan
/// @desc 公共工具类
/// @email 2538096489@qq.com
/// @time 2021/12/21 9:35 上午
class CommonUtils {
  /// @Author swan
  /// @Description 获取屏幕宽度
  /// @Date 9:36 上午 2021/12/21
  static double getScreenWidth(BuildContext context){
    return MediaQuery.of(context).size.width;
  }

  /// @Author swan
  /// @Description 获取屏幕高度
  /// @Date 9:36 上午 2021/12/21
  static double getScreenHeight(BuildContext context){
    return MediaQuery.of(context).size.height;
  }

  /// 关闭app
  static void closeApp(){
    SystemChannels.platform.invokeMethod("SystemNavigator.pop");
  }
}