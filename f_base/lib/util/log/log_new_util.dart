import 'package:flutter/cupertino.dart';

/// @author 强周亮(qiangzhouliang)
/// @desc 打印出全部日志
/// @email 2538096489@qq.com
/// @time 2020/8/28 15:11
class LogNewUtil {
  static const _separator = "=";
  static const _split =
      "$_separator$_separator$_separator$_separator$_separator$_separator$_separator$_separator$_separator";
  static var _title = "Yl-Log";
  static var _isDebug = !const bool.fromEnvironment("dart.vm.product");
  static int _limitLength = 800;
  static String _startLine = "$_split$_title$_split";
  static String _endLine = "$_split$_separator$_separator$_separator$_split";

  static void init({String? title, required bool isDebug,required int limitLength}) {
    _title = title ?? "";
    _isDebug = isDebug;
    _limitLength = limitLength;
    _startLine = "$_split$_title$_split";
    var endLineStr = StringBuffer();
    var cnCharReg = RegExp("[\u4e00-\u9fa5]");
    for (int i = 0; i < _startLine.length; i++) {
      if (cnCharReg.stringMatch(_startLine[i]) != null) {
        endLineStr.write(_separator);
      }
      endLineStr.write(_separator);
    }
    _endLine = endLineStr.toString();
  }

  //仅Debug模式可见
  static void d(dynamic obj) {
    if (_isDebug) {
      _log(obj.toString());
    }
  }

  static void v(dynamic obj) {
    if (_isDebug) {
      _log(obj.toString());
    }
  }

  static void _log(String msg) {
    if (_isDebug) {
      debugPrint(_startLine);
      _logEmpyLine();
      if(msg.length<_limitLength){
        debugPrint(msg);
      }else{
        segmentationLog(msg);
      }
      _logEmpyLine();
      debugPrint(_endLine);
    }
  }

  static void segmentationLog(String msg) {
    if (_isDebug) {
      var outStr = StringBuffer();
      for (var index = 0; index < msg.length; index++) {
        outStr.write(msg[index]);
        if (index % _limitLength == 0 && index!=0) {
          print(outStr);
          outStr.clear();
          var lastIndex = index+1;
          if(msg.length-lastIndex<_limitLength){
            var remainderStr = msg.substring(lastIndex,msg.length);
            print(remainderStr);
            break;
          }
        }
      }
    }
  }

  static void _logEmpyLine(){
    print("");
  }
}
