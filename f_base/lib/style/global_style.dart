import 'package:flutter/material.dart';

/// @author swan
/// @desc 全局样式,标题栏颜色，字体颜色等
/// @email 2538096489@qq.com
/// @time 2020/12/7 14:11
class GlobalStyle {
  static const Color bluefontColor = Color.fromRGBO(38,154,243, 1.0);

  //自定义标题色
  static const Color themeColor = Color(0xff3a8cf0);

  //导航栏返回按钮颜色
  static const Color backBtnColor = Color.fromRGBO(151, 151, 151, 1.0);

  static const TextStyle navTextStyle = TextStyle(fontSize: 18.0, color: Color.fromRGBO(51, 51, 51, 1),fontWeight: FontWeight.w400);
  //线条的默认颜色
  static Color line_color() => const Color(0xffDDDDDD);

  static const Color colorE6E6E6 = Color(0xffE6E6E6);
  static const Color color333333 = Color(0xff333333);
  static const Color color999999 = Color(0xff999999);
  static const Color color666 = Color(0xff666666);
  static const Color white = Color(0xffffffff);

  ///一级标题颜色 0xff333333
  static const Color titleColor = Color(0xff333333);
  ///内容颜色 0xff888888
  static const Color contentColor = Color(0xff888888);
  static const Color hintTextColor =  Color.fromRGBO(158, 162, 169, 1);
  static const Color textBgColor =  Color.fromRGBO(244, 247, 251, 1);
  static const Color typeBgColor =  Color.fromRGBO(235, 235, 235, 1);

  //线条的默认颜色
  static const Color lineColor = Color(0xffDDDDDD);
  //星星颜色，价格颜色
  static const Color priceColor = Color(0xffff9400);
  //按钮的蓝色
  static const Color blueColor_deep = Color(0xff0067d9);
  //按钮的蓝色 比较浅
  static const Color blueColor_shallow = Color(0xff7bb0ea);
  ///蓝色的字体颜色 0xff333333
  static const Color titleBlueColor = Color(0xff495784);
  //默认背景颜色
  // static Color colorbg =  Color(0xffe8e8e8);
  static const Color colorbg =  themeColor;

  static const Color color777777 = Color(0xff777777);
  static const Color red = Color(0xffA90F00);
  static const Color black = Color(0xff000000);
  static const Color divider = Color(0xffF1F1F1);
  static const Color colorHomeBannerDate = Color(0xff674E02);
  static const Color detailPriceColor = Color(0xffFF8C7E);
}