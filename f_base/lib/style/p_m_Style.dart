import 'package:flutter_screenutil/flutter_screenutil.dart';
/// @author 强周亮(qiangzhouliang)
/// @desc padding 和margin公共自定义
/// @email 2538096489@qq.com
/// @time 2020/12/8 10:53
class PMStyle {
  // 每一个对应两个值，w 代表宽 ，h 代表高
  /// 32
  static double maxLargeW = 32.w;
  /// 16
  static double tooLargeW = 16.w;

  /// 14
  static double largeW = 14.w;

  /// 12
  static double tooBigW = 12.w;

  /// 10
  static double bigW = 10.w;

  /// 8
  static double middleW = 8.w;

  /// 6
  static double tooMiddleW = 6.w;

  /// 4
  static double tinyW = 4.w;

  /// 2
  static double tooTinyW = 2.w;
}