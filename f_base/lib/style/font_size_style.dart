import 'package:flutter_screenutil/flutter_screenutil.dart';
/// @author 强周亮(qiangzhouliang)
/// @desc 统一字体大小
/// @email 2538096489@qq.com
/// @time 2020/12/8 09:24
class FontSizeStyle {

  /// 36sp
  static double bigLarge = 36.sp;
  /// 22sp
  static double tooLarge = 22.sp;
  /// 20sp
  static double large = 20.sp;

  /// 18sp
  static double maxBig = 18.sp;
  /// 16sp
  static double big = 16.sp;
  /// 14sp
  static double tooBig = 14.sp;

  /// 12sp
  static double middle = 12.sp;

  /// 10sp
  static double small = 10.sp;

  /// 9 sp
  static double tooSmall = 9.sp;

  /// 8 sp
  static double tiny = 8.sp;



}
