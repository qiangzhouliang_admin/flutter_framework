
class Constants{

  static bool isShowLoading =false;
  //是否是记住密码
  static String isSelectRememberPs = "isSelectRememberPs";
  //输入的密码
  static String passwordStr = "passwordStr";
  //用户信息
  static String userInfo = "userInfo";

  //引导页标记
  static String newFeature = "newFeature";
  //引导页最大次数
  static  int   maxNewFeatureCount = 1;
  static String KEY_GUIDE = "KEY_GUIDE";

  //sputil 是否暗灰模式存储
  static String isGrey = "isGrey";


}