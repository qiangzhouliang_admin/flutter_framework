import 'package:get/get.dart';

/// @author 强周亮(qiangzhouliang)
/// @desc 国际化配置
/// @email 2538096489@qq.com
/// @time 2020/12/11 11:55
class Messages extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
    'zh_CN': {
      'alert': '警告',
      'copy': '复制',
      'paste': '粘贴',
      'cut': '剪切',
      'selectAll': '选择全部',
      'today': '今天'
    },
    'en_US': {
      'alert': 'Alert',
      'copy': 'Copy',
      'paste': 'Paste',
      'cut': 'Cut',
      'selectAll': 'Select all',
      'today': 'today'
    }
  };
}