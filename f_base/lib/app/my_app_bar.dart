import 'package:flutter/material.dart';
import 'package:f_base/base_exp.dart';
import 'package:flutter/services.dart';

/// @author 强周亮(qiangzhouliang)
/// @desc 自定义APPbar
/// @email 2538096489@qq.com
/// @time 2020/6/24 22:37
/// actions:<Widget>[
//        IconButton(
//            icon:Icon(Icons.search),
//            onPressed: (){
//              //调出搜索条
//              showSearch(context: context, delegate: searchBarDelegate());
//            }
//        ),
//      ]
/// tabBar:TabBar(
//              controller: _controller,
//              tabs:[
//                Tab(icon:Icon(Icons.directions_car)),
//                Tab(icon:Icon(Icons.directions_transit)),
//                Tab(icon:Icon(Icons.directions_bike)),
//              ],
//            )
//        )
PreferredSizeWidget myAppBar(context,
    { title = '',
      isShowLead = true,
      leadingIcon,
      VoidCallback? onPressed,
      List<Widget>? actions,
      tabBar,
      Color? tabBarColor,
      Widget? titleWidget,
      systemOverlayStyle = SystemUiOverlayStyle.dark
    }) {
  return AppBar(
      systemOverlayStyle: systemOverlayStyle,
      //返回图标
      leading: isShowLead ? IconButton(
        icon: leadingIcon ?? const Icon(Icons.navigate_before,color:  GlobalStyle.white,),
        onPressed: onPressed ?? () { Navigator.of(context).pop(); },
      ) : Container(),
      titleSpacing: 0,
      /// 配置appbar的阴影高度
      elevation: 0,
      bottom: tabBar != null ? PreferredSize(
        /// 用来配置 bottom 区域的高度
        preferredSize: Size.fromHeight(60.h),
        child: Material(color: tabBarColor ?? GlobalStyle.white, child: tabBar,),
      ) : null,
      //内容居中
      centerTitle: true,
      //标题
      title:titleWidget ?? Text(title,style: TextStyle(color: GlobalStyle.white,fontSize: FontSizeStyle.large),),
      backgroundColor: GlobalStyle.themeColor,
      //右边操作栏，传递数组
      actions:actions
  );
}