import 'dart:io';

import 'package:f_base/util/log/log_util.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:f_base/util/log/logger.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:sp_util/sp_util.dart';

/// @author 强周亮(qiangzhouliang)
/// @desc app 公共的一些操作，如页面初始化等
/// @email 2538096489@qq.com
/// @time 2020/6/24 21:25
class App {
  // release:false  debug:true
  static bool isDebug = !const bool.fromEnvironment("dart.vm.product");


  //app 初始化的一些操作
  static initApp(BuildContext context){
    //设置字体大小根据系统的“字体大小”辅助选项来进行缩放,默认为false
    //默认是1920*1080大小
    //设置尺寸（填写设计中设备的屏幕尺寸）如果设计基于360dp * 690dp的屏幕
    ScreenUtil.init(context, designSize: _getDesignSize());
  }

  /// 设置屏幕适配大小
  static Size _getDesignSize(){
    if(kIsWeb){
      return const Size(1344, 756);
    } else if(Platform.isWindows || Platform.isMacOS){
      return const Size(1344, 756);
    } else {
      return const Size(375, 812);
    }
  }
  static initMain(){
    if(!kIsWeb && (Platform.isAndroid || Platform.isIOS)) {
      /// 设置屏幕方向
      //_initUmeng();
    }
    _initLog();
    _initSpUtil();
  }

  /// android 设置状态栏为透明的沉浸
  static submergence_status_bar() {
    // if (Platform.isAndroid) {
    // 以下两行 设置android状态栏为透明的沉浸。写在组件渲染之后，是为了在渲染后进行set赋值，覆盖状态栏，写在渲染之前MaterialApp组件会覆盖掉这个值。
    SystemUiOverlayStyle systemUiOverlayStyle = const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarBrightness: Brightness.light,
      statusBarIconBrightness: Brightness.light,
    );
    SystemChrome.restoreSystemUIOverlays();
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
    // }
  }


  //初始化友盟统计的一些工作
  /*static _initUmeng(){
    UmengAnalyticsPlugin.init(
      iosKey: "5f1a38eeb4fa6023ce194f84",
      androidKey: "5f1a3894b4fa6023ce194f68",
      channel: "flutter_framework",
      logEnabled: true
    );
  }*/

  //打印日志初始化
  static _initLog(){
    /// 添加console日志打印
    LogUtil.addLogger(PrintLogger(isDebug),isShowLog: isDebug);

    /// 添加debug文件日志
//    LogUtil.addLogger(DebugLogger('dir', 'logName'));
    /// 添加error文件日志
//    LogUtil.addLogger(ErrorLogger('dir', 'logName'));
  }


  static _initSpUtil() async {
    await SpUtil.getInstance();
  }
}