import 'package:f_model/common/token_model.dart';
import 'package:f_model/common/user_model.dart';
import 'package:f_model/update/app_update_entity.dart';
import 'package:get/get.dart';
import 'package:sp_util/sp_util.dart';

import '../controller/LoginStreamController.dart';

/// @author 强周亮(qiangzhouliang)
/// @desc 用户管理类
/// @email 2538096489@qq.com
/// @time 2020/8/11 21:52
class UserManager {
  UserModel? _userModel;
  AppUpdateEntity? _appUpdateEntity;
  TokenModel? tokenModel;

  String? _appVersion;
  late LoginStreamController _loginStreamController;

  // 单例
  factory UserManager() => _getInstance();
  static UserManager get instance => _getInstance();
  static UserManager? _instance;
  UserManager._internal() {
    // 初始化
  }
  static UserManager _getInstance() {
    _instance ??= UserManager._internal();
    return _instance!;
  }


  UserModel get userModel => _userModel!;

  set userModel(UserModel value) {
    _userModel = value;
    SpUtil.putObject("userModel", _userModel!);
    _loginStreamController.isLogin = true;
  }

  void init(){
    _loginStreamController = Get.put(LoginStreamController());
    Map<String,dynamic>? map = SpUtil.getObject("userModel")?.cast<String, dynamic>();
    if(map != null){
      _userModel = UserModel.fromJson(map);
      _loginStreamController.isLogin = true;
    }
  }

  void clear(){
    _userModel = null;
    SpUtil.remove("userModel");
    _loginStreamController.isLogin = false;
  }

  setAppUpdateEntity(AppUpdateEntity entity){
    _appUpdateEntity = entity;
  }

  AppUpdateEntity? getAppUpdateEntity(){
    return _appUpdateEntity;
  }

  setAppVersion(String appVersion){
    _appVersion = appVersion;
  }

  String lastestVersion(){
    if(_appUpdateEntity==null){
      return '';
    }
    return _appUpdateEntity?.version ?? '';
  }

  String? appVersion(){
    return _appVersion ?? '';
  }
}
