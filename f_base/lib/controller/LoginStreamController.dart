import 'package:f_base/base_exp.dart';

/// @Description 用户登录状态 controller
/// @Author swan
/// @Date 2021/12/30 5:25 下午
/// 
class LoginStreamController extends GetxController {
  //是否已登录
  final RxBool _isLogin = false.obs;

  bool get isLogin => _isLogin.value;

  set isLogin(bool value) {
    _isLogin.value = value;
  }
}