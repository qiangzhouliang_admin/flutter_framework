import 'package:f_base/app/constants.dart';
import 'package:get/get.dart';
import 'package:sp_util/sp_util.dart';

/// @author 强周亮(qiangzhouliang)
/// @desc getx 公共controller，存放公共处理的一些状态
/// @email 2538096489@qq.com
/// @time 2020/12/16 18:14
class BaseController extends GetxController {
  //是否全局变灰,true变灰，false 不变灰
  var isGrey = SpUtil.getBool(Constants.isGrey).obs;
}