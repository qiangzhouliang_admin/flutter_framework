import 'package:flutter/material.dart';
import 'package:f_base/base_exp.dart';


/// @author 强周亮(qiangzhouliang)
/// @desc 基础样式操作封装
/// @email 2538096489@qq.com
/// @time 2020/6/30 11:43
class BaseWidgetUtil{
  static const Color _color = Color.fromRGBO(221, 221, 221, 1);
  //展示宽度线
  static Widget line_w({height = 10.0,color,width=1.0}) => Container( height: height,width: width, color: color??GlobalStyle.line_color(),);
  //横线
  static Widget lineBase({height = 0.5,color}) => Container( height: height, color: color??GlobalStyle.line_color(),);

  //虚线
  static Widget dashLine({color = _color}) => MySeparator(color: color);

  /// @desc 按钮样式
  /// @author 强周亮
  /// @time 2020/7/1 9:47
  /// type 按钮类型 1 取消 0 确定，默认 1
  static Widget button(BuildContext context,{
    title="取消",
    bgcolor = Colors.white,
    required textColor,type=1,
    required VoidCallback onPress,
    topLeft = 5.0,topRight = 5.0,bottomLeft = 5.0, bottomRight = 5.0,
    textSize = 14.0}) {
    return ElevatedButton(
      onPressed: onPress,
      child: Text(title),
      style: ElevatedButton.styleFrom(
        backgroundColor: bgcolor,
        textStyle: TextStyle(color: textColor, fontSize: ScreenUtil().setSp(textSize)),
        elevation: 0.0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(topLeft: Radius.circular(topLeft),topRight: Radius.circular(topRight),bottomLeft: Radius.circular(bottomLeft),bottomRight: Radius.circular(bottomRight))),
      ),
    );
    // return RaisedButton(
    //   onPressed: onPress,
    //   child: Text(title,style: TextStyle(fontSize: ScreenUtil().setSp(textSize)),),
    //   color: bgcolor,
    //   textColor: textColor,
    //   elevation:0.0,
    //   shape: RoundedRectangleBorder(
    //       borderRadius: BorderRadius.only(topLeft: Radius.circular(topLeft),topRight: Radius.circular(topRight),bottomLeft: Radius.circular(bottomLeft),bottomRight: Radius.circular(bottomRight))),
    // );
  }

  static Widget nodata(){
    return  Container(
        margin: const EdgeInsets.only(top: 20),
        padding: const EdgeInsets.all(80),
        child: Column(
          children: <Widget>[
            Image.asset("images/icon_no_data.png",fit: BoxFit.fill,),
          ],
        )
    );
  }
}