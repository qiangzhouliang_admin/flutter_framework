import 'package:flutter/material.dart';
import 'package:f_base/base_exp.dart';
class MyButton {
  /// @desc 公共按钮
  /// @author 强周亮
  /// @time 2020/8/28 09:52
  static Widget commonButton(BuildContext context,{
    required VoidCallback onPress,
    width = double.infinity,
    title = '退出登录',
    bgcolor,
    textColor= Colors.white,
    textSize = 14.0,
    highlightColor,
    clickSplashColor,
    height,
    double mLeft = 20,double mRight = 20,double mTop = 20,double mBottom = 20,
  }) {
    return Container(
        height: height ?? 50.h,
//        width: ScreenUtil.screenWidth - 40.w,
        width: width,
        margin: EdgeInsets.only(left:mLeft,right:mRight,top: mTop,bottom: mBottom),
        child: button(context,
            title: title,
            bgcolor: bgcolor??GlobalStyle.themeColor,
            textColor: textColor,
            textSize: textSize,
            onPress: onPress)
    );
  }

  /// @desc 按钮样式
  /// @author 强周亮
  /// @time 2020/7/1 9:47
  /// highlightColor 按钮常按时的颜色
  /// clickSplashColor; //点击时，水波动画中水波的颜色
  static Widget button(BuildContext context,{
    title="取消",
    bgcolor = Colors.white,
    required textColor,
    required VoidCallback onPress,
    topLeft = 5.0,topRight = 5.0,bottomLeft = 5.0, bottomRight = 5.0,
    textSize = 14.0}) {
    return ElevatedButton(
      onPressed: onPress,
      child: Text(title),
      style: ElevatedButton.styleFrom(
        backgroundColor: bgcolor,
        textStyle: TextStyle(color: textColor, fontSize: ScreenUtil().setSp(textSize)),
        elevation: 0.0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(topLeft: Radius.circular(topLeft),topRight: Radius.circular(topRight),bottomLeft: Radius.circular(bottomLeft),bottomRight: Radius.circular(bottomRight))),
      ),
    );
  }
}
