import 'package:flutter/material.dart';

/// 页面保持状态组件
/// 使用：SWKeepAliveWrapper(child: xxxPage())
class SWKeepAliveWrapper extends StatefulWidget{
  final Widget child;

  const SWKeepAliveWrapper({super.key, required this.child});

  @override
  State<StatefulWidget> createState() {
    return SWKeepAliveWrapperState();
  }
  
}
class SWKeepAliveWrapperState extends State<SWKeepAliveWrapper> with AutomaticKeepAliveClientMixin{

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }

  @override
  bool get wantKeepAlive => true;
  
}