import 'package:f_base/widget/text_widget/text_widget.dart';
import 'package:flutter/material.dart';
import 'package:f_base/base_exp.dart';

/// @author 强周亮(qiangzhouliang)
/// @desc 暂无数据样式
/// @email 2538096489@qq.com
/// @time 2020/12/8 15:49
class NoDataWidget {
  static Widget getNoData({lrPadding,tbPadding,String tipContent = ''}) {
    return Center(child:
       Column(
         mainAxisAlignment: MainAxisAlignment.center,
         children: [
           Image.asset(Res.icon_no_data,fit: BoxFit.fill,width: 200.w,height: 200.w,),
           TextWidget.commonText(content: tipContent,color: GlobalStyle.contentColor,fontSize: FontSizeStyle.tooBig)
         ],
       )
    );
  }
}
