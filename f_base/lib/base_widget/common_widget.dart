import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:f_base/base_exp.dart';

/// @author 强周亮(qiangzhouliang)
/// @desc 公共组件封装
/// @email 2538096489@qq.com
/// @time 2021/1/15 09:17
class CommonWidget {


  /// @desc gridView 统一封装
  /// @author 强周亮
  /// @time 2021/1/15 09:40
  static Widget commonGridViewWidget({listNew,
    int crossAxisCount = 2,
    double mainAxisSpacing = 4.0,
    double crossAxisSpacing = 4.0,
    double childAspectRatio = 1.7,
    Axis scrollDirection = Axis.vertical,
    isEnableScroll = false
  }){
    return GridView.builder(
        itemCount: listNew.length,
        physics: isEnableScroll ? null : const NeverScrollableScrollPhysics() ,
        //是否允许内容适配
        shrinkWrap: true,
        scrollDirection: scrollDirection,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: crossAxisCount,
            mainAxisSpacing: mainAxisSpacing,
            crossAxisSpacing: crossAxisSpacing,
            childAspectRatio: childAspectRatio
        ),
        itemBuilder: (context,index){
          return listNew[index];
        }
    );
  }

  /// @desc 圆角Container
  /// @author 强周亮
  /// @time 2021/1/19 10:46
  static Widget radiusContainer({
    double? width,                   ///宽度，默认80
    double? height,                  ///高度，默认80
    Widget? widget,                   ///里面包含的内容
    double? padding,                  ///padding值，默认10
    Color? bgColor,                    ///背景颜色，默认白色
    double? radius,                     ///圆角半径,默认 10
    GestureTapCallback? onTap,
    AlignmentGeometry alignment = Alignment.center,
    double mTop = 0,
    double mLeft = 0,
    double mRight = 0,
    double mBottom = 0
  }) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
          width: width ?? 80.w,
          height: height ?? 80.w,
          margin: EdgeInsets.only(top: mTop,bottom: mBottom,left: mLeft, right: mRight),
          padding: EdgeInsets.all(padding ?? 10),
          alignment: alignment,
          decoration: BoxDecoration(color: bgColor ?? GlobalStyle.white, borderRadius: BorderRadius.circular(radius ?? 10.w)),
          child: widget
      ),
    );
  }
}