import 'package:f_base/style/global_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/// @Description 按钮组件
/// @Author swan
/// @Date 2022/5/22 13:51
/// 
class ButWidgetU {
  /// @desc 按钮样式
  /// @author 强周亮
  /// @time 2020/7/1 9:47
  static Widget button({
    required VoidCallback onPress,
    title="取消",
    Color bgcolor = GlobalStyle.themeColor,
    Color textColor = Colors.white,
    elevation = 0.0,
    topLeft = 5.0,topRight = 5.0,bottomLeft = 5.0, bottomRight = 5.0,
    textSize = 16.0}) {
    return ElevatedButton(
      onPressed: onPress,
      style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(bgcolor),
          elevation: MaterialStateProperty.all(elevation),
          shape:MaterialStateProperty.all(RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(topLeft),
                  topRight: Radius.circular(topRight),
                  bottomLeft: Radius.circular(bottomLeft),
                  bottomRight: Radius.circular(bottomRight))))
      ),
      child: Text(title,style: TextStyle(fontSize: ScreenUtil().setSp(textSize),color: textColor),),
    );
  }

  /// @Author swan
  /// @Description 字体按钮
  /// @Date 14:02 2022/5/22
  ///
  static Widget textBut(title,{
    required VoidCallback onPress,
    Color textColor = GlobalStyle.themeColor,
    textSize = 16.0
  }){
    return TextButton(onPressed: onPress, child: Text(title,style: TextStyle(fontSize: ScreenUtil().setSp(textSize),color: textColor),));
  }
  /// @Author swan
  /// @Description 带边框的按钮
  /// @Date 14:02 2022/5/22
  ///
  static Widget outLineBut(title,{
    required VoidCallback onPress,
    Color textColor = GlobalStyle.themeColor,
    textSize = 16.0,
    Color bgColor = GlobalStyle.white,
    borderWidth = 1,
    borderColor = GlobalStyle.themeColor,
    elevation = 0.0
  }){
    return OutlinedButton(
      onPressed: onPress,
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(bgColor),
        side: MaterialStateProperty.all(BorderSide(width: ScreenUtil().setWidth(borderWidth),color: borderColor)),
        elevation: MaterialStateProperty.all(elevation),
      ),
      child: Text(title,style: TextStyle(fontSize: ScreenUtil().setSp(textSize),color: textColor),),
    );
  }
}