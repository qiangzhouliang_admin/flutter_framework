import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';


///自定义文本输入框
class TextFormFieldWidget extends StatelessWidget {
  //占位提示文本
  final String? hintText;
  final String? initText;

  //输入框前置图标
  final IconData? prefixIconData;

  //输入框后置图标
  final IconData? suffixIconData;

  //是否隐藏文本
  final bool? obscureText;
  final bool? autoValidate;

  //输入实时回调
  final Function(String value)? onChanged;
  final FormFieldValidator<String>? validator;
  final TextEditingController? controller;
  final FocusNode? focusNode;
  final Function(String? value)? submit,saved;
  final Function()? onTap;

  TextFormFieldWidget({
    Key? key,
    this.initText = '',
    this.hintText = '',
    this.submit,
    this.saved,
    this.validator,
    this.focusNode,
    this.prefixIconData,
    this.suffixIconData,
    this.obscureText,
    this.autoValidate,
    this.onChanged,
    this.controller,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //构建输入框
    return TextFormField(
      initialValue: initText,
      focusNode: focusNode,
      controller: controller,
      onFieldSubmitted: submit,
      onSaved: saved,
      //是否隐藏文本
      obscureText: obscureText ?? false,
      //隐藏文本小圆点的颜色
      cursorColor: Theme.of(context).colorScheme.secondary,
      //文本样式
      style: TextStyle(
        color: Theme.of(context).colorScheme.secondary,
        fontSize: 14.sp,
      ),
      autovalidateMode: autoValidate == true ? AutovalidateMode.always : AutovalidateMode.disabled,
      validator: validator,
      //输入框的边框
      decoration: InputDecoration(
        //提示文本
        labelText: hintText,
        hintText: '请输入${hintText!}',
        //提示文本的样式
        labelStyle: TextStyle(color: Theme.of(context).colorScheme.secondary),
        //可编辑时的提示文本的颜色
        focusColor: Theme.of(context).colorScheme.secondary,
        //填充
        filled: true,
        //可编辑时 无边框样式
        enabledBorder: const UnderlineInputBorder(
          borderSide: BorderSide.none,
        ),
        //获取输入焦点时的边框样式
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: BorderSide(color: Theme.of(context).colorScheme.secondary),
        ),

        //文本前置的图标
        prefixIcon: prefixIconData != null ? Icon(
          prefixIconData,
          size: 18,
          color: Theme.of(context).colorScheme.secondary,
        ) : null,
        //文本后置的图标
        suffixIcon: suffixIconData != null ?  GestureDetector(
          onTap: onTap,
          child: Icon(
            suffixIconData,
            size: 18,
            color: Theme.of(context).colorScheme.secondary,
          ) ,
        ) : null,
      ),
    );
  }
}
