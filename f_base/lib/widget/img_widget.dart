import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../res.dart';

/// @Description 图片控件自定义
/// @Author swan
/// @Date 2022/5/23 14:50
/// 
class ImgWidget {
  /// @desc 网络缓存图片库组件封装
  /// @author 强周亮
  /// @time 2021/1/15 09:11
  static Widget cacheNetImgWidget(String imgUrl,{
    double? width,
    double? height,
    bool cache = true,
    double radiusNum = 10.0,    //圆角大小
    BoxFit fit = BoxFit.contain
  }) {
    if((imgUrl.startsWith("https://") || imgUrl.startsWith("http://"))){
      return ExtendedImage.network(
        imgUrl,
        width: width,
        height: height,
        fit: fit,
        cache: cache,
        //不设置shape时，下面的两个属性不起作用
        shape: BoxShape.rectangle,
        // border: Border.all(color: Colors.red, width: 1.0),
        borderRadius: BorderRadius.all(Radius.circular(radiusNum)),
        loadStateChanged: (ExtendedImageState state){
          /*if(state.extendedImageLoadState == LoadState.loading){
            return Container(
              alignment: Alignment.center,
              child: Image.asset("images/loading.gif", fit: BoxFit.fill,),
            );
          } else */
          if(state.extendedImageLoadState == LoadState.failed){
            return Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(5.w),
              child: Image.asset(Res.icon_no_data, fit: BoxFit.cover),
            );
          } else {
            return null;
          }
        },
      );
    } else if(imgUrl != null){
      return Image.asset(imgUrl, fit: fit, width: width ?? 20.w, height: height ?? 20.w);
    } else {
      return Container();
    }

  }
}