import 'package:f_base/style/font_size_style.dart';
import 'package:f_base/style/global_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/// @Description listTitle 瓦片组件自定义
/// @Author swan
/// @Date 2022/5/23 14:40
/// 
class ListTitleU{
  /// @Author swan
  /// @Description 公共listtitle
  /// @Date 10:23 2022/5/23
  ///
  static Widget cListTitle(String title,{
    Widget? leading,
    String? subTitle,
    bool isShowRightIcon = true,
    GestureTapCallback? onTap
  }){
    return Container(
      decoration: const BoxDecoration(
          border: Border(bottom: BorderSide(color: GlobalStyle.lineColor))
      ),
      child: ListTile(
        leading: leading,
        title: Text(title,style: TextStyle(color: GlobalStyle.titleColor,fontWeight: FontWeight.w500,fontSize: FontSizeStyle.big),),
        subtitle: subTitle?.isNotEmpty == true ? Text(title,style: TextStyle(color: GlobalStyle.contentColor,fontSize: FontSizeStyle.tooBig),) : null,
        trailing: isShowRightIcon ? Icon(Icons.arrow_forward_ios,color: GlobalStyle.contentColor,size: 16.sp,) : null,
        dense: true,//此列表图块是否是垂直密集列表的一部分，如果是true文本将会更小
        onTap: onTap,
      ),
    );
  }
}