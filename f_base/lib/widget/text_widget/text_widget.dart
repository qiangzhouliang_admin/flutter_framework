import 'package:f_base/style/font_size_style.dart';
import 'package:f_base/style/global_style.dart';
import 'package:flutter/material.dart';

/// @Description : 文本组件
/// @Author swan
/// @Date 2022/5/23 14:52
/// 
class TextWidget {
  /// @desc 公共text
  /// @author 强周亮
  /// @time 2021/1/19 15:31
  static Text commonText({
    content = '',
    Color? color,                 //字体颜色，默认白色
    int? maxLines = 3,
    double? fontSize,                     //字体大小，默认14sp
    fontWeight = FontWeight.normal,
    TextAlign? textAlign
  }){
    return Text(content ?? '',
      maxLines: maxLines,
      softWrap: false,
      overflow: TextOverflow.ellipsis,
      textAlign: textAlign,
      style: TextStyle(
          color: color ?? GlobalStyle.white,
          fontSize: fontSize ?? FontSizeStyle.tooBig,
          fontWeight: fontWeight
      ),);
  }
}