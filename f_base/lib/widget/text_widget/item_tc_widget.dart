import 'package:f_base/style/global_style.dart';
import 'package:f_base/widget/text_widget/text_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

/// @author swan
/// @desc 内容动态设置监听控制器
/// @email 2538096489@qq.com
/// @time 2022/5/23 17:46
class ItemTCController extends GetxController{
  // 判断是否超过 1 行
  final RxBool _isGTSignLine = false.obs;

  // 判断是否超过 多行 ，做点击展开 收藏效果
  final RxBool _isGTMoreLine = false.obs;

  bool get isGTSignLine => _isGTSignLine.value;

  set isGTSignLine(bool value) {
    _isGTSignLine.value = value;
  }

  bool get isGTMoreLine => _isGTMoreLine.value;

  set isGTMoreLine(bool value) {
    _isGTMoreLine.value = value;
  }
}

/// @Description 左右形式的内容样式
/// @Author swan
/// @Date 2022/5/23 16:00
/// 
class ItemTcWidget extends StatelessWidget {
  // 标题
  String title;
  // 内容
  String? content;
  // 标题颜色和内容颜色
  Color titleColor,contentColor,bgColor;
  // 间距 默认 8dp
  double pLeft,pRight,pTop,pBottom,pTRight;
  // 字体大小 默认14sp
  double? titleSize,contentSize;
  // 最大可展示多少行
  int? maxLine;

  late Text cont,contTitle;
  late final ItemTCController _itemTCController = Get.put(ItemTCController());

  ItemTcWidget({Key? key,
      required this.title,
      this.content,
      this.titleColor = GlobalStyle.titleColor,
      this.contentColor = GlobalStyle.contentColor,
      this.bgColor = GlobalStyle.white,
      this.pLeft = 8.0,
      this.pRight = 8.0,
      this.pTop = 8.0,
      this.pBottom = 8.0,
      this.pTRight = 8.0,
      this.maxLine = 3,
      this.titleSize = 14.0,
      this.contentSize = 14.0
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    contTitle = TextWidget.commonText(content: title,color: titleColor,fontSize: titleSize);
    cont = TextWidget.commonText(content: content ?? '',color: contentColor, textAlign: TextAlign.right,fontSize: contentSize);
    isGTSignLine(title,content ?? '', cont);
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(pLeft),
          top: ScreenUtil().setWidth(pTop),
          right: ScreenUtil().setWidth(pRight),
          bottom: ScreenUtil().setWidth(pBottom)),
      decoration: BoxDecoration(
        border: const Border(bottom: BorderSide(color: GlobalStyle.lineColor)),
        color: bgColor
      ),
      child: InkWell(
        onTap: () => _itemTCController.isGTMoreLine = !_itemTCController.isGTMoreLine,
        child: Row(
          children: [
            Padding(padding: EdgeInsets.only(right: ScreenUtil().setWidth(pTRight)),child: contTitle,),
            Expanded(child: Obx(() => TextWidget.commonText(
                content: content ?? '',
                color: contentColor,
                textAlign: _itemTCController.isGTSignLine ? TextAlign.left : TextAlign.right,
                maxLines: _itemTCController.isGTMoreLine ? 1000 : maxLine,
                fontSize: titleSize))
            ),
          ],
        ),
      ),
    );
  }

  /// @Author swan
  /// @Description 判断是否超过了一行: true 是 false 否
  /// @Date 16:49 2022/5/23
  ///
  void isGTSignLine(String title,String content,Text txt,{int? maxLine = 1}) {
    // 测量标题
    TextPainter textTitle = TextPainter(text: TextSpan(text: title),
        textDirection: TextDirection.ltr)
      ..layout(maxWidth: 1.sw, minWidth: 50);

    // 测量内容
    TextPainter textPainter = TextPainter(
        maxLines: maxLine,
        text: TextSpan(
            text: content, style:txt.style),
        textDirection: TextDirection.ltr)
      ..layout(maxWidth: 1.sw - pLeft - pTRight - pRight - textTitle.width , minWidth: 50);

    //判断 文本是否需要截断
    _itemTCController.isGTSignLine = textPainter.didExceedMaxLines;

  }
}