import 'package:flutter/material.dart';
import 'package:f_base/base_exp.dart';
import 'package:f_base/easyrefresh/footer.dart';
import 'package:f_base/easyrefresh/header.dart';
import 'package:easy_refresh/easy_refresh.dart';

/// 加载类型枚举 load 加载，LOADMORE加载更多
enum LoadType {
  LOAD,LOADMORE
}

///请求成功时的接口回调
///type = 1 加载数据，type = 2 加载更多
typedef SuccessCallBack(LoadType type,data);
/// 请求失败时的接口回调
/// type = 1 加载数据，type = 2 加载更多
typedef ErrorCallBack(LoadType type,error);

/// @author 强周亮(qiangzhouliang)
/// @desc listview 封装，带上拉加载和下拉刷新功能
/// @email 2538096489@qq.com
/// @time 2020/8/17 18:32
class UploadRefreshListWidget {
  final SuccessCallBack onSuccess;
  final ErrorCallBack onError;
  //当前页
  int pageNo = 1;
  int totalPage = 0;
  int pageSize = 15;

  final ScrollController _scrollController = ScrollController();
  BuildContext context;
  //是否显示正在加载页面
  var isShowLoading = true;
  //网络请求URL
  var url;
  var pageNoKey;
  var PageSizeKey;
  var requestContentType;
  /// 是否加密
  var isAes;
  var baseUrl;
  SliverToBoxAdapter? topWidget;
  SliverPersistentHeader? floatWidget;
  //请求参数
  Map<String, dynamic>? params;
  final EasyRefreshController _controller = EasyRefreshController(
    controlFinishLoad: true,
    controlFinishRefresh: true
  );
  void setParams(Map<String, dynamic> params){
    this.params = params;
    getData();
  }

  UploadRefreshListWidget({
    required this.onSuccess,
    required this.onError,
    required this.url,
    required this.context,
    this.pageNoKey = 'pageNo',
    this.PageSizeKey = 'pageSize',
    this.pageSize = 15,
    this.params,
    this.totalPage = 1,
    this.requestContentType,
    this.isAes = true,
    this.baseUrl,
    this.topWidget,      //顶部内容
    this.floatWidget     //浮动内容
  });

  Widget listView(BuildContext context,{List? contentListData,enableRefresh = true, enableLoad = true}) {
    this.context = context;
    return Expanded(
      child: EasyRefresh(
        controller: _controller,
        scrollController: _scrollController,
        header: enableRefresh ? header(context):null,
        footer: enableLoad? footer() : null,
        onRefresh: enableRefresh ? () async {
          isShowLoading = false;
          getData();
        } : null,
        onLoad: enableLoad ? () async {
          isShowLoading = false;
          if(pageNo <= totalPage) {
            loadMordData();
          }
        } : null,
        child: CustomScrollView(
          slivers: sliversContent(contentListData),
        ),
      ),
    );
  }

  List<Widget> sliversContent(List? contentListData) {
    List<Widget> list = [];
    //头部内容
    if(topWidget != null){
      list.add(topWidget!);
    }

    //头部以下悬浮内容
    if(floatWidget != null){
      list.add(floatWidget!);
    }

    //内容部分
    list.add(SliverList(
      delegate: SliverChildBuilderDelegate((context, index) {
        if(contentListData != null && contentListData.isNotEmpty){
          return contentListData[index];
        } else {
          return NoDataWidget.getNoData();
        }
      },
        childCount: contentListData?.length == 0 ? 1 : contentListData?.length,
      ),
    ));

    return list;
  }



  /// @desc 加载数据
  /// @author 强周亮
  /// @time 2020/8/18 10:05
  void getData(){
    pageNo = 1;
    requestData(LoadType.LOAD);
  }
  ///加载更多
  void loadMordData(){
    requestData(LoadType.LOADMORE);
  }
  /// @desc 请求数据
  /// @author 强周亮
  /// @time 2020/8/18 10:26
  void requestData(LoadType loadType) async {
    if(params == null){
      params = {
        pageNoKey:pageNo.toString(),
        PageSizeKey:pageSize.toString()
      };
    } else {
      params![pageNoKey] = pageNo.toString();
      params![PageSizeKey] = pageSize.toString();
    }
    ResponseInfo responseInfo = await DioUtils.instance.getRequest(url: url,queryParameters: params);
    if(loadType == LoadType.LOAD) {
      _controller.resetHeader();
    }

    _controller.resetFooter();
    _controller.finishRefresh();
    //获取总共有多少页的数据
    if(responseInfo.success){
      //获取总共有多少页的数据
      var result = responseInfo.data;
      //_pageModel = PageModel.fromJson(result);
      //初始化分页加载所需的数据
      //totalPage = _pageModel?.totalPageCount ?? 0;

      onSuccess(loadType,result);
    } else {
      onError(loadType,responseInfo.message);
      EasyLoading.showToast(responseInfo.message);
    }
  }
}
