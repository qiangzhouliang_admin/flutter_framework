import 'package:f_base/base_exp.dart';
import 'package:flutter/material.dart';
import 'package:m_pc_manager/controller/MenuController.dart' as MC;

import 'HeaderIconWidget.dart';
import 'HeaderTitleWidget.dart';

/// 博客头部,顶部的菜单，小于 800 像素时，影藏，显示左拉菜单
class HeaderWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HeaderWidgetState();
  }
}

class HeaderWidgetState extends State<HeaderWidget> {
  // 绑定注入控制器
  final MC.MenuController _menuController = Get.put(MC.MenuController());

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            padding: const EdgeInsets.only(left: 20, right: 20),
            //最大宽度
            //constraints: BoxConstraints(maxWidth: 1000),
            //填充
            width: double.infinity,
            child: Column(
              //包裹
              mainAxisSize: MainAxisSize.min,
              children: [
                // 第一行标题
                Container(
                  //内边距
                  padding: const EdgeInsets.only(top: 20,bottom: 20),
                  //最大宽度
                  constraints: const BoxConstraints(maxWidth: 1000),
                  child: buildRow(),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  /// 第一行标题栏
  Row buildRow() {
    return Row(
      children: [
        buildLeftMenu(),
        const Text(
          "后端管理系统",
          style: TextStyle(
              fontWeight: FontWeight.w600, fontSize: 22, color: Colors.red),
        ),
        //中间的分类
        Expanded(
          child: buildMiddleRow(),
        ),
        //右侧的联系我们
        Row(
          children: [
            //小图标
            buildRightImage(),
            const SizedBox(width: 10,),
            //联系我们
            ElevatedButton(onPressed: () {}, child: const Text("联系我们"))
          ],
        )
      ],
    );
  }

  Widget buildRightImage(){
    if(CommonUtils.getScreenWidth(context) <= 580){
      return Container();
    }
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        HeaderIconWidget(Res.qq_icon)
      ],
    );
  }

  /// 顶部菜单中间部分-菜单栏
  Widget buildMiddleRow() {
    if (CommonUtils.getScreenWidth(context) <= 1000) {
      return Container();
    }
    //观察者
    return Obx((){
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ...List.generate(_menuController.menuList.length,
                  (index) => HeaderTitleWidget(
                      _menuController.menuList[index],
                      _menuController.currentSelectIndex == index,
                    onTab: (){
                        _menuController.updateSelectIndex(index);
                    },
                  )),
        ],
      );
    });
  }

  /// 左侧的菜单按钮
  buildLeftMenu() {
    if (CommonUtils.getScreenWidth(context) >= 1000) {
      return Container();
    }
    return IconButton(
      onPressed: () {
        _menuController.homeScaffoldKey.currentState?.openDrawer();
      },
      icon: const Icon(Icons.menu),
      color: Colors.white,
    );
  }
}
