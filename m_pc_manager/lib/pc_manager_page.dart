import 'package:f_base/base_exp.dart';
import 'package:flutter/material.dart';

import 'controller/MenuController.dart' as MC;
import 'header/header_widget.dart';
import 'menu/SlideMenuWidget.dart';
/// 后台管理系统
class PcManagerPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return PcManagerPageState();
  }
  
}
class PcManagerPageState extends State<PcManagerPage>{
  // 绑定注入控制器
  final MC.MenuController _menuController = Get.put(MC.MenuController());
  @override
  Widget build(BuildContext context) {
    App.initApp(context);
    return Scaffold(
      //定义一个key，控制侧拉页面的展开与收缩
      key: _menuController.homeScaffoldKey,
      backgroundColor: Colors.grey[200],
      //侧拉页面
      drawer: SlideMenuWidget(),
      /// 页面整体滑动
      body: SingleChildScrollView(
        child: Column(children: [
          // 第一部分标题
          HeaderWidget(),
          // 第二部分内容部分
          Obx((){
            return Text(_menuController.menuList[_menuController.currentSelectIndex]);
          })
        ],),
      ),
    );
  }
  
}
