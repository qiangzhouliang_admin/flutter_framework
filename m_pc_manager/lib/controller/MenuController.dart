import 'package:f_base/base_exp.dart';
import 'package:flutter/material.dart';
/// 菜单controller
class MenuController extends GetxController{

  @override
  void onInit(){
    super.onInit();
  }
  //首页左滑菜单控制key
  GlobalKey<ScaffoldState> _homeScaffoldKey = GlobalKey();
  GlobalKey<ScaffoldState> get homeScaffoldKey => _homeScaffoldKey;

  // 当前那个菜单被选中
  RxInt _currentSelectIndex = 0.obs;
  int get currentSelectIndex => _currentSelectIndex.value;
  void updateSelectIndex(int index) {
    _currentSelectIndex.value = index;
  }

  //菜单列表
  final List<String> _nemuList = [
    "应用管理",
    "角色管理",
    "菜单权限",
    "字典管理",
  ];

  List<String> get menuList => _nemuList;

}